﻿using Domain;
using System;
using System.Net;
using System.Net.Mail;
using WebApp;

namespace Services
{
    public static class Email
    {
        public static bool Enviar(string pSubject, string pBody, User pUser) {

            var result = false;

            try {

                var fromAddress = new MailAddress("info@awarener.com", "AWARENER");
                var toAddress = new MailAddress(pUser.Email, pUser.Name);
                string fromPassword = "Schiltron77";
                string subject = pSubject;
                string body = pBody;

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }

                result = true;

            } catch (Exception ex)
            {
                Program.logger.Error("Error envio de mail");
                result = false;
            }

            return result;

        }

    }
}
