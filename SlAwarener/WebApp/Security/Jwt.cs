﻿using Newtonsoft.Json;
using System;
using System.Security.Cryptography;
using System.Text;

namespace WebApp.Security
{
    public static class Jwt
    {
        public static string  Key = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
        public static string Token { get; set; }

        public static JwtHeader oJwtHeader;
        public static JwtPayload oJwtPayload;

        public static string CreateToken(string pUserName, string pUserPass, string pUserEmail)
        {
            oJwtHeader = new JwtHeader();
            oJwtPayload = new JwtPayload();

            oJwtHeader.Alg = "jwt";
            oJwtHeader.Type = "HMACSHA256";

            oJwtPayload.UserName = pUserName;
            oJwtPayload.UserPass = pUserPass;
            oJwtPayload.UserEmail = pUserEmail;

            var oJwtHeaderAux = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(oJwtHeader));
            var oJwtPayloadAux = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(oJwtPayload));

            Token = Convert.ToBase64String(oJwtHeaderAux) + "." + Convert.ToBase64String(oJwtPayloadAux);

            byte[] signature;

            var crypto = new HMACSHA256(Encoding.UTF8.GetBytes(Key));

            signature = crypto.ComputeHash(Encoding.UTF8.GetBytes(Token));

            return Token += "." + Convert.ToBase64String(signature);
        }


        public static JwtPayload DecodeToken(string token)
        {
            var segments = token.Split('.');

            if (segments.Length != 3)
                throw new Exception("Token structure is incorrect!");

            JwtHeader oJwtHeaderAux = JsonConvert.DeserializeObject<JwtHeader>(Encoding.UTF8.GetString(Convert.FromBase64String(segments[0])));
            JwtPayload oJwtPayloadAux = JsonConvert.DeserializeObject<JwtPayload>(Encoding.UTF8.GetString(Convert.FromBase64String(segments[1])));

            byte[] signature;
            var crypto = new HMACSHA256(Encoding.UTF8.GetBytes(Key));
            signature = crypto.ComputeHash(Encoding.UTF8.GetBytes(segments[0] + "." + segments[1]));

            var tokeparameter = segments[0] + "." + segments[1] + "." + Convert.ToBase64String(signature);
            if (tokeparameter != token)
                throw new Exception("Token incorrect!");

            return oJwtPayloadAux;
        }
    }

    public class JwtHeader
    {
        public String Alg { get; set; }
        public String Type { get; set; }
    }

    public class JwtPayload
    {
        public String UserName { get; set; }
        public String UserPass { get; set; }
        public String UserEmail { get; set; }
        public int Exp { get; set; }
    }


}
