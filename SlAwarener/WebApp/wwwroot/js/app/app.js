﻿
var config = {};

var app = angular.module("MyApp", ['ngCookies', 'ngDialog', 'ngRoute', 'angularUtils.directives.dirPagination']);

app.config(function ($routeProvider) {

    config.baseURL = "http://localhost:58742/";

    $routeProvider.when('/Tabs', {

        templateUrl: config.baseURL + "Tabs/Index",
        controller: 'tabsController'
    })

    $routeProvider.when('/Company', {
        templateUrl: config.baseURL + "Company/Index",
        controller: 'companyController'
    })

});



