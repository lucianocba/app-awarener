﻿app.controller('tabsController', function ($scope, $http, ngDialog) {

    $scope.onMensaje = function () {
        ngDialog.open({ template: 'templateId' });
    };

    $scope.getTabs = function () {
        $http.get("/Tabs/GetTabs", null).then(function (response) {

            $scope.lstTabs = response.data;
        });
    }

    $scope.getTabs();

});