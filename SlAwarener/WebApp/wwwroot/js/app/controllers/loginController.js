﻿app.controller("loginController", function ($scope, $http, $cookies, $window) {

    $scope.onLogin = function ()
    {
        $http.post("/Login/Login", $scope.selectItem)
            .then(function (response)
            {
                window.location.href = '/Home';
            });
    }


    $scope.ClearData = function ()
    {
        $scope.email = "";
        $scope.password = "";
    }

    $scope.ClearData();
});