﻿using ApiProviders;
using ApiProviders.Intrinio;
using log4net;
using log4net.Config;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Reflection;
using System.Xml;

namespace WebApp
{
    public class Program
    {
        public static ILog logger = null;

        public static void Main(string[] args)
        {



            // seteamos la configuracion del log
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());

            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

            logger = LogManager.GetLogger(typeof(Program));

            //seteamos la api que vamos a usar
            WebApi.ApiClient = new Intrinio();

            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
