﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Services;
using WebApp.Security;

namespace WebApp.Controllers
{
    public class RegisterController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public void Register([FromBody] User data)
        {
            try
            {
                if (data != null)
                {
                    //guardar el usuario

                    var token = Jwt.CreateToken(data.Name, data.Password, data.Email);

                    //enviar  mail de verificacion
                    var resultEnvio = Email.Enviar("Register","Gracias por registrarse en Awarener: http:///localhost:58742//Register//Autenticar?key=" + token, data);

                    data.add();

                }

            }
            catch (Exception ex)
            {
                Program.logger.Error("error Register",ex);
            }



        }
    }
}