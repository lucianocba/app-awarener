﻿using ApiProviders;
using Domain;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApp.Security;

namespace WebApp.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Index()
        {
            // uso de las interfaces para obtener todas las companias, por fefecto esa usa intrinio se carga en la clase program
            var lstCompanias = WebApi.ApiClient.GetAllTags();
            
            //uso de log4net
            Program.logger.Error("esto es un error en login Controller");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] User parameterLogin)
        {
           
            if (parameterLogin.Name == "a" && parameterLogin.Password == "a")
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, "jon", ClaimValueTypes.String, "https://yourdomain.com")
                };

                var userIdentity = new ClaimsIdentity(claims, "SecureLogin");
                var userPrincipal = new ClaimsPrincipal(userIdentity);

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                    userPrincipal,
                    new AuthenticationProperties
                    {
                        ExpiresUtc = DateTime.UtcNow.AddMinutes(20),
                        IsPersistent = false,
                        AllowRefresh = false
                    });

                    return RedirectToAction("Index", "Home");
                }

            return RedirectToAction("Index", "Home");
        }

        

        public class Param {

            public string userName { get; set; }
            public string passWord { get; set; }
        }

    }
}