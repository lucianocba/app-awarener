﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class TabsController : Controller
    {
        [Authorize]
        public IActionResult Index()
        {
            return PartialView();
        }

        [Authorize]
        [HttpGet]
        public List<Tag> GetTabs()
        {
           return Tag.GetAllTags();
        }

    }
}