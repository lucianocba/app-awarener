﻿using ApiProviders.Interfaces;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ApiProviders
{
    public static class WebApi
    {
        private static HttpClient httpClient;
        public static IApiProvider ApiClient;

        public static string BuildCompleteUri(string relativePath)
        {
            return Client.BaseAddress.ToString() + relativePath;
        }

        public static HttpClient Client
        {
            get
            {
                if (httpClient == null)
                {
                    httpClient = new HttpClient();
                    Client.BaseAddress = new Uri(ApiClient.GetUrlApi()); 

                    Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(String.Format("{0}:{1}", ApiClient.GetUserAPi(), ApiClient.GetPassApi()))));
                }

                return httpClient;
            }
        }

        public static async Task<string> GetAsync(string path)
        {
            try
            {
                String completeUri = BuildCompleteUri(path);
                HttpResponseMessage apiResponse = await Client.GetAsync(completeUri);
                var data = string.Empty;

                if (apiResponse.IsSuccessStatusCode)
                {
                    string responseData = apiResponse.Content.ReadAsStringAsync().Result;
                    if (!string.IsNullOrWhiteSpace(responseData))
                    {
                        data = responseData;
                    }
                }
                else
                {
                    data = string.Empty;
                }

                return data;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
