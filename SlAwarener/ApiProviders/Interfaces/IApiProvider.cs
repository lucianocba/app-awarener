﻿using ApiProviders.Intrinio;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiProviders.Interfaces
{
    public interface IApiProvider
    {
          string GetUrlApi();
          string GetUserAPi();
          string GetPassApi();

          List<Tag> GetAllTags();
          Task<Company> GetUniqueCompany(string pNameComapny);
          Task<ApiResponse<CompanyNews>> GetCompanyNews(string pticker);
          Task<ApiResponse<Price>> GetPrices(string pNameComapny);
          Task<List<Company>> GetAllCompanies();
          Task<List<Company>> GetAllCompanyInfo(List<Company> lstCompany);
          Task<List<CompanySECFill>> GetSECFill(string pticker);
          Task<ApiResponse<StandarizedData>> GetAllStandarizedData(string pCompany, string pYear, string pStatement, string pFiscalPeriod);
    }
}
