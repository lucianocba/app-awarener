﻿using System;

namespace ApiProviders.Intrinio
{
    public class CurrentLimit
    {
        public string access_code { get; set; }
        public Int32 current { get; set; }
        public Int32 limit { get; set; }
        public Int32 percent { get; set; }
    }
}
