﻿using ApiProviders.Interfaces;
using Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ApiProviders.Intrinio
{

    public enum EnumAccessCodes
    {
        com_sec_master_data, // - Global Public Company Security Master Data Feed
        com_fin_data, // - US Public Company Financials Data Feed
        industry_data, // - US Sector & Industry Data Feed
        fdic_data, // - US Bank & Bank Holding Company Data Feed
        econ_data, // - US & Global Economic Data Feed
        insider_trans, // - US Insider Transactions & Ownership Data Feed
        inst_hold //- US Institutional Holdings Data Feed
    }


    public class Intrinio : IApiProvider
    {
        public async Task<Company> GetUniqueCompany(string pNameComapny)
        {
            try
            {
                var response = await WebApi.GetAsync("companies?identifier=" + pNameComapny);
                Company responseDataObject = JsonConvert.DeserializeObject<Company>(response);

                return responseDataObject;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ApiResponse<CompanyNews>> GetCompanyNews(string pticker)
        {
            try
            {
                var response = await WebApi.GetAsync("news?identifier=" + pticker);
                ApiResponse<CompanyNews> responseDataObject = JsonConvert.DeserializeObject<ApiResponse<CompanyNews>>(response);

                return responseDataObject;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ApiResponse<Price>> GetPrices(string pNameComapny)
        {
            try
            {
                var response = await WebApi.GetAsync("prices?identifier=" + pNameComapny);
                ApiResponse<Price> responseDataObject = JsonConvert.DeserializeObject<ApiResponse<Price>>(response);

                return responseDataObject;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Company>> GetAllCompanies()
        {
            try
            {
                List<Company> lstCompany = new List<Company>();

                var response = await WebApi.GetAsync("companies");
                ApiResponse<Company> responseDataObject = JsonConvert.DeserializeObject<ApiResponse<Company>>(response);

                var current_page = responseDataObject.current_page;
                var total_page = responseDataObject.total_pages;
                var page_size = responseDataObject.page_size;

                foreach (var company in responseDataObject.data)
                {
                    lstCompany.Add(company);
                }

                do
                {
                    current_page = current_page + 1;

                    response = await WebApi.GetAsync("companies?page_number=" + current_page);
                    responseDataObject = JsonConvert.DeserializeObject<ApiResponse<Company>>(response);

                    foreach (var company in responseDataObject.data)
                    {
                        lstCompany.Add(company);
                    }

                } while (current_page <= total_page);

                var resultlist = lstCompany.Where(x => x.Ticker != null);

                var lstCompanyDto = await GetAllCompanyInfo(resultlist.ToList());

                return lstCompanyDto;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<List<Company>> GetAllCompanyInfo(List<Company> lstCompany)
        {
            try
            {
                List<Company> lstDto = new List<Company>();

                foreach (var company in lstCompany)
                {
                    var response = await WebApi.GetAsync("companies?identifier=" + company.Ticker);
                    Company responseDataObject = JsonConvert.DeserializeObject<Company>(response);

                    lstDto.Add(responseDataObject);
                }

                return lstDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<CurrentLimit> GetCurrentCredit(EnumAccessCodes pEnumAccessCode)
        {
            try
            {
                var response = await WebApi.GetAsync("usage/current?access_code=" + pEnumAccessCode.ToString());
                CurrentLimit responseDataObject = JsonConvert.DeserializeObject<CurrentLimit>(response);

                return responseDataObject;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<TotalDailyLimit> GetTotalDailyLimite(EnumAccessCodes pEnumAccessCode)
        {
            try
            {
                var response = await WebApi.GetAsync("usage/access");
                List<TotalDailyLimit> responseDataObject = JsonConvert.DeserializeObject<List<TotalDailyLimit>>(response);

                var LstTotalDailyLmit = responseDataObject.Where(x => x.access_code == pEnumAccessCode.ToString());

                if (LstTotalDailyLmit != null)
                    return LstTotalDailyLmit.FirstOrDefault();
                else
                    return null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<CompanySECFill>> GetSECFill(string pticker)
        {
            try
            {
                List<CompanySECFill> lstCompanySECFillDto = new List<CompanySECFill>();
                ApiResponse<CompanySECFill> responseDataObject;

                var response = await WebApi.GetAsync("filings?identifier=" + pticker);
                responseDataObject = JsonConvert.DeserializeObject<ApiResponse<CompanySECFill>>(response);

                var current_page = responseDataObject.current_page;
                var total_page = responseDataObject.total_pages;
                var page_size = responseDataObject.page_size;

                foreach (var secfill in responseDataObject.data)
                {
                    lstCompanySECFillDto.Add(secfill);
                }

                do
                {
                    current_page = current_page + 1;

                    response = await WebApi.GetAsync("filings?identifier=" + pticker + "?page_number=" + current_page);
                    responseDataObject = JsonConvert.DeserializeObject<ApiResponse<CompanySECFill>>(response);

                    foreach (var secfill in responseDataObject.data)
                    {
                        lstCompanySECFillDto.Add(secfill);
                    }

                } while (current_page <= total_page);

                return lstCompanySECFillDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ApiResponse<StandarizedData>> GetAllStandarizedData(string pCompany, string pYear, string pStatement, string pFiscalPeriod)
        {
            try
            {
                List<StandarizedData> lstSTData = new List<StandarizedData>();

                var response = await WebApi.GetAsync(String.Format("financials/standardized?identifier={0}&statement={1}&fiscal_year={2}&fiscal_period={3}", pCompany, pStatement, pYear, pFiscalPeriod));
                ApiResponse<StandarizedData> responseDataObject = JsonConvert.DeserializeObject<ApiResponse<StandarizedData>>(response);

                return responseDataObject;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetUrlApi()
        {
            return "https://api.intrinio.com/";
        }

        public string GetUserAPi()
        {
            return "5cb1fb30c9d61a4dc831c41328de9f60";
        }

        public string GetPassApi()
        {
            return "e588bf8ff7ce68facf6405636f5c14aa";
        }

        public List<Tag> GetAllTags()
        {
            try
            {
                List<Tag> lstSTTags = new List<Tag>();
                StreamReader file;
                
                var path = Environment.CurrentDirectory;
            
                file = File.OpenText(path + "/files/tags.csv");

                while (!file.EndOfStream)
                {
                    var line = file.ReadLine();
                    var financialtags = line.Split(';');

                    Tag oSTInsTags = new Tag();

                    oSTInsTags.Name = financialtags[0];
                    oSTInsTags.TagValue = financialtags[1];
                    oSTInsTags.StatementCode = financialtags[2];
                    oSTInsTags.Template = financialtags[3];
                    oSTInsTags.Balance = financialtags[4];
                    oSTInsTags.Type = financialtags[5];
                    oSTInsTags.Units = financialtags[6];

                    lstSTTags.Add(oSTInsTags);
                }

                file.Close();
                return lstSTTags;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
