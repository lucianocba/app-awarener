﻿using System;

namespace ApiProviders.Intrinio
{
    public class TotalDailyLimit
    {
       public string access_code { get; set; }
       public Int32  daily_limit  { get; set; }
    }
}
