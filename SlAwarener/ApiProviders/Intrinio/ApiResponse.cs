﻿using System.Collections.Generic;

namespace ApiProviders.Intrinio
{
    public class ApiResponse<T> where T : class
    {
        public List<T> data { get; set; }
        public int result_count { get; set; }
        public int page_size { get; set; }
        public int current_page { get; set; }
        public int total_pages { get; set; }
        public int api_call_credits { get; set; }
        public bool successfull { get; set; }
    }
}
