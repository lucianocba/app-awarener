using System;
using System.Collections.Generic;

namespace Domain
{

    public class Price
    {
        public static string FIELD_PRICEID = "priceId";
        public static string FIELD_COMPANYID = "companyId";
        public static string FIELD_ACTIVE = "active";
        public static string FIELD_ADJCLOSE = "adjClose";
        public static string FIELD_ADJHIGH = "adjHigh";
        public static string FIELD_ADJLOW = "adjLow";
        public static string FIELD_ADJOPEN = "adjOpen";
        public static string FIELD_ADJVOLUME = "adjVolume";
        public static string FIELD_CLOSING = "closing";
        public static string FIELD_DATE_CREATION = "date_creation";
        public static string FIELD_DATE_DATE = "date_date";
        public static string FIELD_EXDIVIDEND = "exDividend";
        public static string FIELD_HIGH = "high";
        public static string FIELD_LOW = "low";
        public static string FIELD_OPENING = "opening";
        public static string FIELD_SPLITRATIO = "splitRatio";
        public static string FIELD_DATE_UPDATED = "date_updated";
        public static string FIELD_VOLUME = "volume";



        private long priceId;
        public long PriceId { get { return priceId; } }

        private long companyId;
        public long CompanyId { get { return companyId; } set { companyId = value; } }

        private int active;
        public int Active { get { return active; } set { active = value; } }

        private double adjClose;
        public double AdjClose { get { return adjClose; } set { adjClose = value; } }

        private double adjHigh;
        public double AdjHigh { get { return adjHigh; } set { adjHigh = value; } }

        private double adjLow;
        public double AdjLow { get { return adjLow; } set { adjLow = value; } }

        private double adjOpen;
        public double AdjOpen { get { return adjOpen; } set { adjOpen = value; } }

        private double adjVolume;
        public double AdjVolume { get { return adjVolume; } set { adjVolume = value; } }

        private double closing;
        public double Closing { get { return closing; } set { closing = value; } }

        private DateTime dateCreation;
        public DateTime DateCreation { get { return dateCreation; } set { dateCreation = value; } }

        private DateTime dateDate;
        public DateTime DateDate { get { return dateDate; } set { dateDate = value; } }

        private double exDividend;
        public double ExDividend { get { return exDividend; } set { exDividend = value; } }

        private double high;
        public double High { get { return high; } set { high = value; } }

        private double low;
        public double Low { get { return low; } set { low = value; } }

        private double opening;
        public double Opening { get { return opening; } set { opening = value; } }

        private double splitRatio;
        public double SplitRatio { get { return splitRatio; } set { splitRatio = value; } }

        private DateTime dateUpdated;
        public DateTime DateUpdated { get { return dateUpdated; } set { dateUpdated = value; } }

        private double volume;
        public double Volume { get { return volume; } set { volume = value; } }

        private static DAL.DAL_Price dal = new DAL.DAL_Price();

        public void add()
        {
            this.priceId = dal.Add_Price(
                this.CompanyId,
                this.Active,
                (decimal)this.AdjClose,
                (decimal)this.AdjHigh,
                (decimal)this.AdjLow,
                (decimal)this.AdjOpen,
                (decimal)this.AdjVolume,
                (decimal)this.Closing,
                this.DateCreation,
                this.DateDate,
                (decimal)this.ExDividend,
                (decimal)this.High,
                (decimal)this.Low,
                (decimal)this.Opening,
                (decimal)this.SplitRatio,
                this.DateUpdated,
                (decimal)this.Volume);
        }

        public void update()
        {
            dal.Update_Price(
                this.PriceId,
                this.CompanyId,
                this.Active,
                (decimal)this.AdjClose,
                (decimal)this.AdjHigh,
                (decimal)this.AdjLow,
                (decimal)this.AdjOpen,
                (decimal)this.AdjVolume,
                (decimal)this.Closing,
                this.DateCreation,
                this.DateDate,
                (decimal)this.ExDividend,
                (decimal)this.High,
                (decimal)this.Low,
                (decimal)this.Opening,
                (decimal)this.SplitRatio,
                this.DateUpdated,
                (decimal)this.Volume);
        }

        public void delete()
        {
            dal.Delete_Price(this.PriceId);
        }

        public static void delete(long priceId)
        {
            dal.Delete_Price(priceId);
        }

        public bool getById(long priceId)
        {
            System.Data.DataSet ds = dal.Get_By_Id_Price(priceId);
            return fillStandardizedDataMembers(ref ds);
        }

        private bool fillStandardizedDataMembers(ref System.Data.DataSet aDataSet)
        {
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                if (tempTable.Rows.Count > 0)
                {
                    System.Data.DataRow drow = tempTable.Rows[0];
                    fillObjectFromDataRow(drow, this);
                    return true;
                }
            }
            return false;
        }

        private static List<Price> fillStandardizedData(ref System.Data.DataSet aDataSet)
        {
            List<Price> ret = new List<Price>();
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                for (int i = 0; i < tempTable.Rows.Count; ++i)
                {
                    System.Data.DataRow drow = tempTable.Rows[i];
                    Price temp = new Price();
                    fillObjectFromDataRow(drow, temp);
                    ret.Add(temp);
                }
            }
            return ret;
        }

        private static void fillObjectFromDataRow(System.Data.DataRow drow, Price temp)
        {
            temp.priceId = Convert.ToInt64(drow[DAL.DAL_Price.PRICEID]);
            temp.CompanyId = Convert.ToInt64(drow[DAL.DAL_Price.COMPANYID]);
            temp.Active = Convert.ToInt32(drow[DAL.DAL_Price.ACTIVE]);
            temp.AdjClose = Convert.ToDouble(drow[DAL.DAL_Price.ADJCLOSE]);
            temp.AdjHigh = Convert.ToDouble(drow[DAL.DAL_Price.ADJHIGH]);
            temp.AdjLow = Convert.ToDouble(drow[DAL.DAL_Price.ADJLOW]);
            temp.AdjOpen = Convert.ToDouble(drow[DAL.DAL_Price.ADJOPEN]);
            temp.AdjVolume = Convert.ToDouble(drow[DAL.DAL_Price.ADJVOLUME]);
            temp.Closing = Convert.ToDouble(drow[DAL.DAL_Price.CLOSING]);
            if (string.IsNullOrEmpty(drow[DAL.DAL_Price.DATE_CREATION].ToString()) == false)
            {
                String tempDate = drow[DAL.DAL_Price.DATE_CREATION].ToString().Substring(0, 19);
                temp.DateCreation = DateTime.ParseExact(tempDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                temp.DateCreation = DateTime.Now;
            }
            if (string.IsNullOrEmpty(drow[DAL.DAL_Price.DATE_DATE].ToString()) == false)
            {
                String tempDate = drow[DAL.DAL_Price.DATE_DATE].ToString().Substring(0, 19);
                temp.DateDate = DateTime.ParseExact(tempDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                temp.DateDate = DateTime.Now;
            }
            temp.ExDividend = Convert.ToDouble(drow[DAL.DAL_Price.EXDIVIDEND]);
            temp.High = Convert.ToDouble(drow[DAL.DAL_Price.HIGH]);
            temp.Low = Convert.ToDouble(drow[DAL.DAL_Price.LOW]);
            temp.Opening = Convert.ToDouble(drow[DAL.DAL_Price.OPENING]);
            temp.SplitRatio = Convert.ToDouble(drow[DAL.DAL_Price.SPLITRATIO]);
            if (string.IsNullOrEmpty(drow[DAL.DAL_Price.DATE_UPDATED].ToString()) == false)
            {
                String tempDate = drow[DAL.DAL_Price.DATE_UPDATED].ToString().Substring(0, 19);
                temp.DateUpdated = DateTime.ParseExact(tempDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                temp.DateUpdated = DateTime.Now;
            }
            temp.Volume = Convert.ToDouble(drow[DAL.DAL_Price.VOLUME]);
        }

        public static List<Price> search(string priceId, string companyId, string active, string adjClose, string adjHigh, string adjLow, string adjOpen, string adjVolume, string closing, string date_creationFrom, string date_creationTo, string date_dateFrom, string date_dateTo, string exDividend, string high, string low, string opening, string splitRatio, string date_updatedFrom, string date_updatedTo, string volume, string sortingField)
        {
            System.Data.DataSet ds = dal.Select_Price(priceId, companyId, active, adjClose, adjHigh, adjLow, adjOpen, adjVolume, closing, date_creationFrom, date_creationTo, date_dateFrom, date_dateTo, exDividend, high, low, opening, splitRatio, date_updatedFrom, date_updatedTo, volume, sortingField);
            return fillStandardizedData(ref ds);
        }
    }
}
