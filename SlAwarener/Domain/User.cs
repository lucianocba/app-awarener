﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class User
    {
        public static string FIELD_USERID = "userId";
        public static string FIELD_NAME = "name";
        public static string FIELD_LASTNAME = "lastName";
        public static string FIELD_COUNTRY = "country";
        public static string FIELD_STATE = "state";
        public static string FIELD_CITY = "city";
        public static string FIELD_DATE_BIRTHDAY = "date_birthday";
        public static string FIELD_PASSWORD = "password";
        public static string FIELD_EMAIL = "email";
        public static string FIELD_PROFESSION = "profession";

        private long userId;
        public long UserId { get { return userId; } }

        private string name;
        public string Name { get { return name; } set { name = value; } }

        private string lastName;
        public string LastName { get { return lastName; } set { lastName = value; } }

        private string country;
        public string Country { get { return country; } set { country = value; } }

        private string state;
        public string State { get { return state; } set { state = value; } }

        private string city;
        public string City { get { return city; } set { city = value; } }

        private DateTime dateBirthday;
        public DateTime DateBirthday { get { return dateBirthday; } set { dateBirthday = value; } }

        private string password;
        public string Password { get { return password; } set { password = value; } }

        private string email;
        public string Email { get { return email; } set { email = value; } }

        private string profession;
        public string Profession { get { return profession; } set { profession = value; } }

        private static DAL.DAL_User dal = new DAL.DAL_User();

        public void add()
        {
            this.userId = dal.Add_User(
                this.Name,
                this.LastName,
                this.Country,
                this.State,
                this.City,
                this.DateBirthday,
                this.Password,
                this.Email,
                this.Profession);
        }

        public void update()
        {
            dal.Update_User(
                this.UserId,
                this.Name,
                this.LastName,
                this.Country,
                this.State,
                this.City,
                this.DateBirthday,
                this.Password,
                this.Email,
                this.Profession);
        }

        public void delete()
        {
            dal.Delete_User(this.UserId);
        }

        public static void delete(long userId)
        {
            dal.Delete_User(userId);
        }

        public bool getById(long userId)
        {
            System.Data.DataSet ds = dal.Get_By_Id_User(userId);
            return fillStandardizedDataMembers(ref ds);
        }

        private bool fillStandardizedDataMembers(ref System.Data.DataSet aDataSet)
        {
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                if (tempTable.Rows.Count > 0)
                {
                    System.Data.DataRow drow = tempTable.Rows[0];
                    fillObjectFromDataRow(drow, this);
                    return true;
                }
            }
            return false;
        }

        private static List<User> fillStandardizedData(ref System.Data.DataSet aDataSet)
        {
            List<User> ret = new List<User>();
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                for (int i = 0; i < tempTable.Rows.Count; ++i)
                {
                    System.Data.DataRow drow = tempTable.Rows[i];
                    User temp = new User();
                    fillObjectFromDataRow(drow, temp);
                    ret.Add(temp);
                }
            }
            return ret;
        }

        private static void fillObjectFromDataRow(System.Data.DataRow drow, User temp)
        {
            temp.userId = Convert.ToInt64(drow[DAL.DAL_User.USERID]);
            temp.Name = Convert.ToString(drow[DAL.DAL_User.NAME]);
            temp.LastName = Convert.ToString(drow[DAL.DAL_User.LASTNAME]);
            temp.Country = Convert.ToString(drow[DAL.DAL_User.COUNTRY]);
            temp.State = Convert.ToString(drow[DAL.DAL_User.STATE]);
            temp.City = Convert.ToString(drow[DAL.DAL_User.CITY]);
            if (string.IsNullOrEmpty(drow[DAL.DAL_User.DATE_BIRTHDAY].ToString()) == false)
            {
                temp.DateBirthday = DateTime.ParseExact(Convert.ToString(drow[DAL.DAL_User.DATE_BIRTHDAY].ToString()), "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                temp.DateBirthday = DateTime.Now;
            }
            temp.Password = Convert.ToString(drow[DAL.DAL_User.PASSWORD]);
            temp.Email = Convert.ToString(drow[DAL.DAL_User.EMAIL]);
            temp.Profession = Convert.ToString(drow[DAL.DAL_User.PROFESSION]);
        }

        public static List<User> search(string userId, string name, string lastName, string country, string state, string city, string date_birthdayFrom, string date_birthdayTo, string password, string email, string profession, string sortingField)
        {
            System.Data.DataSet ds = dal.Select_User(userId, name, lastName, country, state, city, date_birthdayFrom, date_birthdayTo, password, email, profession, sortingField);
            return fillStandardizedData(ref ds);
        }
    }
}
