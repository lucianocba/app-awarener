using System;
using System.Collections.Generic;

namespace Domain
{

    public class StandarizedData
    {

        public static string FIELD_STANDARIZEDDATAID = "standarizedDataId";
        public static string FIELD_ACTIVE = "active";
        public static string FIELD_DATE_CREATION = "date_creation";
        public static string FIELD_FISCALPERIOD = "fiscalPeriod";
        public static string FIELD_FISCALYEAR = "fiscalYear";
        public static string FIELD_IDCOMPANY = "idCompany";
        public static string FIELD_STATEMENT = "statement";
        public static string FIELD_TAG = "tag";
        public static string FIELD_DATE_UPDATED = "date_updated";
        public static string FIELD_VALUE = "value";

        private long standarizedDataId;
        public long StandarizedDataId { get { return standarizedDataId; } }

        private int active;
        public int Active { get { return active; } set { active = value; } }

        private DateTime dateCreation;
        public DateTime DateCreation { get { return dateCreation; } set { dateCreation = value; } }

        private string fiscalPeriod;
        public string FiscalPeriod { get { return fiscalPeriod; } set { fiscalPeriod = value; } }

        private short fiscalYear;
        public short FiscalYear { get { return fiscalYear; } set { fiscalYear = value; } }

        private long idCompany;
        public long IdCompany { get { return idCompany; } set { idCompany = value; } }

        private string statement;
        public string Statement { get { return statement; } set { statement = value; } }

        private string tag;
        public string Tag { get { return tag; } set { tag = value; } }

        private DateTime dateUpdated;
        public DateTime DateUpdated { get { return dateUpdated; } set { dateUpdated = value; } }

        private double dataValue;
        public double Value { get { return dataValue; } set { dataValue = value; } }

        private static DAL.DAL_StandarizedData dal = new DAL.DAL_StandarizedData();

        public void add()
        {
            this.standarizedDataId = dal.Add_StandarizedData(
                this.Active,
                this.DateCreation,
                this.FiscalPeriod,
                this.FiscalYear,
                this.IdCompany,
                this.Statement,
                this.Tag,
                this.DateUpdated,
                (decimal)this.Value);
        }

        public void update()
        {
            dal.Update_StandarizedData(
                this.StandarizedDataId,
                this.Active,
                this.DateCreation,
                this.FiscalPeriod,
                this.FiscalYear,
                this.IdCompany,
                this.Statement,
                this.Tag,
                this.DateUpdated,
                (decimal)this.Value);
        }

        public void delete()
        {
            dal.Delete_StandarizedData(this.StandarizedDataId);
        }

        public static void delete(long standarizedDataId)
        {
            dal.Delete_StandarizedData(standarizedDataId);
        }

        public bool getById(long standarizedDataId)
        {
            System.Data.DataSet ds = dal.Get_By_Id_StandarizedData(standarizedDataId);
            return fillStandardizedDataMembers(ref ds);
        }

        private bool fillStandardizedDataMembers(ref System.Data.DataSet aDataSet)
        {
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                if (tempTable.Rows.Count > 0)
                {
                    System.Data.DataRow drow = tempTable.Rows[0];
                    fillObjectFromDataRow(drow, this);
                    return true;
                }
            }
            return false;
        }

        private static List<StandarizedData> fillStandardizedData(ref System.Data.DataSet aDataSet)
        {
            List<StandarizedData> ret = new List<StandarizedData>();
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                for (int i = 0; i < tempTable.Rows.Count; ++i)
                {
                    System.Data.DataRow drow = tempTable.Rows[i];
                    StandarizedData temp = new StandarizedData();
                    fillObjectFromDataRow(drow, temp);
                    ret.Add(temp);
                }
            }
            return ret;
        }

        private static void fillObjectFromDataRow(System.Data.DataRow drow, StandarizedData temp)
        {
            temp.standarizedDataId = Convert.ToInt64(drow[DAL.DAL_StandarizedData.STANDARIZEDDATAID]);
            temp.Active = Convert.ToInt32(drow[DAL.DAL_StandarizedData.ACTIVE]);
            if (string.IsNullOrEmpty(drow[DAL.DAL_StandarizedData.DATE_CREATION].ToString()) == false)
            {
                String tempDate = drow[DAL.DAL_StandarizedData.DATE_CREATION].ToString().Substring(0, 19);
                temp.DateCreation = DateTime.ParseExact(tempDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                temp.DateCreation = DateTime.Now;
            }
            temp.FiscalPeriod = Convert.ToString(drow[DAL.DAL_StandarizedData.FISCALPERIOD]);
            temp.FiscalYear = Convert.ToInt16(drow[DAL.DAL_StandarizedData.FISCALYEAR]);
            temp.IdCompany = Convert.ToInt64(drow[DAL.DAL_StandarizedData.IDCOMPANY]);
            temp.Statement = Convert.ToString(drow[DAL.DAL_StandarizedData.STATEMENT]);
            temp.Tag = Convert.ToString(drow[DAL.DAL_StandarizedData.TAG]);
            if (string.IsNullOrEmpty(drow[DAL.DAL_StandarizedData.DATE_UPDATED].ToString()) == false)
            {
                String tempDate = drow[DAL.DAL_StandarizedData.DATE_UPDATED].ToString().Substring(0, 19);
                temp.DateUpdated = DateTime.ParseExact(tempDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                temp.DateUpdated = DateTime.Now;
            }
            temp.Value = Convert.ToDouble(drow[DAL.DAL_StandarizedData.VALUE]);
        }

        public static List<StandarizedData> search(string standarizedDataId, string active, string date_creationFrom, string date_creationTo, string fiscalPeriod, string fiscalYear, string idCompany, string statement, string tag, string date_updatedFrom, string date_updatedTo, string value, string sortingField)
        {
            System.Data.DataSet ds = dal.Select_StandarizedData(standarizedDataId, active, date_creationFrom, date_creationTo, fiscalPeriod, fiscalYear, idCompany, statement, tag, date_updatedFrom, date_updatedTo, value, sortingField);
            return fillStandardizedData(ref ds);
        }
    }
}
