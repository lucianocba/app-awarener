using System;
using System.Collections.Generic;

namespace Domain
{

    public class CompanySECFill
    {
        public static string FIELD_COMPANYSECFILLID = "companySECFillId";
        public static string FIELD_COMPANYID = "companyId";
        public static string FIELD_DATE_ACCEPTEDDATE = "date_acceptedDate";
        public static string FIELD_ACTIVE = "active";
        public static string FIELD_DATE_CREATION = "date_creation";
        public static string FIELD_FILINGURL = "filingUrl";
        public static string FIELD_DATE_FILLINGDATE = "date_fillingDate";
        public static string FIELD_INSTANCEURL = "instanceUrl";
        public static string FIELD_DATE_PERIODENDED = "date_periodEnded";
        public static string FIELD_REPORTTYPE = "reportType";
        public static string FIELD_REPORTURL = "reportUrl";
        public static string FIELD_DATE_UPDATED = "date_updated";

        private long companySECFillId;
        public long CompanySECFillId { get { return companySECFillId; } }

        private long companyId;
        public long CompanyId { get { return companyId; } set { companyId = value; } }

        private DateTime dateAcceptedDate;
        public DateTime DateAcceptedDate { get { return dateAcceptedDate; } set { dateAcceptedDate = value; } }

        private int active;
        public int Active { get { return active; } set { active = value; } }

        private DateTime dateCreation;
        public DateTime DateCreation { get { return dateCreation; } set { dateCreation = value; } }

        private string filingUrl;
        public string FilingUrl { get { return filingUrl; } set { filingUrl = value; } }

        private DateTime dateFillingDate;
        public DateTime DateFillingDate { get { return dateFillingDate; } set { dateFillingDate = value; } }

        private string instanceUrl;
        public string InstanceUrl { get { return instanceUrl; } set { instanceUrl = value; } }

        private DateTime datePeriodEnded;
        public DateTime DatePeriodEnded { get { return datePeriodEnded; } set { datePeriodEnded = value; } }

        private string reportType;
        public string ReportType { get { return reportType; } set { reportType = value; } }

        private string reportUrl;
        public string ReportUrl { get { return reportUrl; } set { reportUrl = value; } }

        private DateTime dateUpdated;
        public DateTime DateUpdated { get { return dateUpdated; } set { dateUpdated = value; } }

        private static DAL.DAL_CompanySECFill dal = new DAL.DAL_CompanySECFill();

        public void add()
        {
            this.companySECFillId = dal.Add_CompanySECFill(
                this.CompanyId,
                this.DateAcceptedDate,
                this.Active,
                this.DateCreation,
                this.FilingUrl,
                this.DateFillingDate,
                this.InstanceUrl,
                this.DatePeriodEnded,
                this.ReportType,
                this.ReportUrl,
                this.DateUpdated);
        }

        public void update()
        {
            dal.Update_CompanySECFill(
                this.CompanySECFillId,
                this.CompanyId,
                this.DateAcceptedDate,
                this.Active,
                this.DateCreation,
                this.FilingUrl,
                this.DateFillingDate,
                this.InstanceUrl,
                this.DatePeriodEnded,
                this.ReportType,
                this.ReportUrl,
                this.DateUpdated);
        }

        public void delete()
        {
            dal.Delete_CompanySECFill(this.CompanySECFillId);
        }

        public static void delete(long companySECFillId)
        {
            dal.Delete_CompanySECFill(companySECFillId);
        }

        public bool getById(long companySECFillId)
        {
            System.Data.DataSet ds = dal.Get_By_Id_CompanySECFill(companySECFillId);
            return fillStandardizedDataMembers(ref ds);
        }

        private bool fillStandardizedDataMembers(ref System.Data.DataSet aDataSet)
        {
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                if (tempTable.Rows.Count > 0)
                {
                    System.Data.DataRow drow = tempTable.Rows[0];
                    fillObjectFromDataRow(drow, this);
                    return true;
                }
            }
            return false;
        }

        private static List<CompanySECFill> fillStandardizedData(ref System.Data.DataSet aDataSet)
        {
            List<CompanySECFill> ret = new List<CompanySECFill>();
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                for (int i = 0; i < tempTable.Rows.Count; ++i)
                {
                    System.Data.DataRow drow = tempTable.Rows[i];
                    CompanySECFill temp = new CompanySECFill();
                    fillObjectFromDataRow(drow, temp);
                    ret.Add(temp);
                }
            }
            return ret;
        }

        private static void fillObjectFromDataRow(System.Data.DataRow drow, CompanySECFill temp)
        {
            temp.companySECFillId = Convert.ToInt64(drow[DAL.DAL_CompanySECFill.COMPANYSECFILLID]);
            temp.CompanyId = Convert.ToInt64(drow[DAL.DAL_CompanySECFill.COMPANYID]);
            if (string.IsNullOrEmpty(drow[DAL.DAL_CompanySECFill.DATE_ACCEPTEDDATE].ToString()) == false)
            {
                String tempDate = drow[DAL.DAL_CompanySECFill.DATE_ACCEPTEDDATE].ToString().Substring(0, 19);
                temp.DateAcceptedDate = DateTime.ParseExact(tempDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                temp.DateAcceptedDate = DateTime.Now;
            }
            temp.Active = Convert.ToInt32(drow[DAL.DAL_CompanySECFill.ACTIVE]);
            if (string.IsNullOrEmpty(drow[DAL.DAL_CompanySECFill.DATE_CREATION].ToString()) == false)
            {
                String tempDate = drow[DAL.DAL_CompanySECFill.DATE_CREATION].ToString().Substring(0, 19);
                temp.DateCreation = DateTime.ParseExact(tempDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                temp.DateCreation = DateTime.Now;
            }
            temp.FilingUrl = Convert.ToString(drow[DAL.DAL_CompanySECFill.FILINGURL]);
            if (string.IsNullOrEmpty(drow[DAL.DAL_CompanySECFill.DATE_FILLINGDATE].ToString()) == false)
            {
                String tempDate = drow[DAL.DAL_CompanySECFill.DATE_FILLINGDATE].ToString().Substring(0, 19);
                temp.DateFillingDate = DateTime.ParseExact(tempDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                temp.DateFillingDate = DateTime.Now;
            }
            temp.InstanceUrl = Convert.ToString(drow[DAL.DAL_CompanySECFill.INSTANCEURL]);
            if (string.IsNullOrEmpty(drow[DAL.DAL_CompanySECFill.DATE_PERIODENDED].ToString()) == false)
            {
                String tempDate = drow[DAL.DAL_CompanySECFill.DATE_PERIODENDED].ToString().Substring(0, 19);
                temp.DatePeriodEnded = DateTime.ParseExact(tempDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                temp.DatePeriodEnded = DateTime.Now;
            }
            temp.ReportType = Convert.ToString(drow[DAL.DAL_CompanySECFill.REPORTTYPE]);
            temp.ReportUrl = Convert.ToString(drow[DAL.DAL_CompanySECFill.REPORTURL]);
            if (string.IsNullOrEmpty(drow[DAL.DAL_CompanySECFill.DATE_UPDATED].ToString()) == false)
            {
                String tempDate = drow[DAL.DAL_CompanySECFill.DATE_UPDATED].ToString().Substring(0, 19);
                temp.DateUpdated = DateTime.ParseExact(tempDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                temp.DateUpdated = DateTime.Now;
            }
        }

        public static List<CompanySECFill> search(string companySECFillId, string companyId, string date_acceptedDateFrom, string date_acceptedDateTo, string active, string date_creationFrom, string date_creationTo, string filingUrl, string date_fillingDateFrom, string date_fillingDateTo, string instanceUrl, string date_periodEndedFrom, string date_periodEndedTo, string reportType, string reportUrl, string date_updatedFrom, string date_updatedTo, string sortingField)
        {
            System.Data.DataSet ds = dal.Select_CompanySECFill(companySECFillId, companyId, date_acceptedDateFrom, date_acceptedDateTo, active, date_creationFrom, date_creationTo, filingUrl, date_fillingDateFrom, date_fillingDateTo, instanceUrl, date_periodEndedFrom, date_periodEndedTo, reportType, reportUrl, date_updatedFrom, date_updatedTo, sortingField);
            return fillStandardizedData(ref ds);
        }
    }
}
