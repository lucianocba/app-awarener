﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class Company
    {

        public static string FIELD_COMPANYID = "companyId";
        public static string FIELD_TEMPLATE = "template";
        public static string FIELD_TICKER = "ticker";
        public static string FIELD_ACTIVE = "active";
        public static string FIELD_BUSINESSADDRESS = "businessAddress";
        public static string FIELD_BUSINESSPHONENBR = "businessPhoneNbr";
        public static string FIELD_CEO = "ceo";
        public static string FIELD_CIK = "cik";
        public static string FIELD_COMPANYURL = "companyUrl";
        public static string FIELD_COUNTRY = "country";
        public static string FIELD_DATE_CREATION = "date_creation";
        public static string FIELD_EMPLOYEES = "employees";
        public static string FIELD_INCCOUNTRY = "incCountry";
        public static string FIELD_INCSTATE = "incState";
        public static string FIELD_INDUSTRYCATEGORY = "industryCategory";
        public static string FIELD_INDUSTRYGROUP = "industryGroup";
        public static string FIELD_LEGALNAME = "legalName";
        public static string FIELD_LEI = "lei";
        public static string FIELD_DESCRIPTION = "Description";
        public static string FIELD_MAILINGADDRESS = "mailingAddress";
        public static string FIELD_NAME = "name";
        public static string FIELD_SECTOR = "sector";
        public static string FIELD_SHORTDESCRIPTION = "shortDescription";
        public static string FIELD_SIC = "sic";
        public static string FIELD_STANDARIZEDACTIVE = "standarizedActive";
        public static string FIELD_STATE = "state";
        public static string FIELD_STOCKEXCHANGE = "stockExchange";

        private long companyId;
        public long CompanyId { get { return companyId; } }

        private string template;
        public string Template { get { return template; } set { template = value; } }

        private string ticker;
        public string Ticker { get { return ticker; } set { ticker = value; } }

        private int active;
        public int Active { get { return active; } set { active = value; } }

        private string businessAddress;
        public string BusinessAddress { get { return businessAddress; } set { businessAddress = value; } }

        private string businessPhoneNbr;
        public string BusinessPhoneNbr { get { return businessPhoneNbr; } set { businessPhoneNbr = value; } }

        private string ceo;
        public string Ceo { get { return ceo; } set { ceo = value; } }

        private string cik;
        public string Cik { get { return cik; } set { cik = value; } }

        private string companyUrl;
        public string CompanyUrl { get { return companyUrl; } set { companyUrl = value; } }

        private string country;
        public string Country { get { return country; } set { country = value; } }

        private DateTime dateCreation;
        public DateTime DateCreation { get { return dateCreation; } set { dateCreation = value; } }

        private int? employees;
        public int? Employees { get { return employees; } set { employees = value; } }

        private string incCountry;
        public string IncCountry { get { return incCountry; } set { incCountry = value; } }

        private string incState;
        public string IncState { get { return incState; } set { incState = value; } }

        private string industryCategory;
        public string IndustryCategory { get { return industryCategory; } set { industryCategory = value; } }

        private string industryGroup;
        public string IndustryGroup { get { return industryGroup; } set { industryGroup = value; } }

        private string legalName;
        public string LegalName { get { return legalName; } set { legalName = value; } }

        private string lei;
        public string Lei { get { return lei; } set { lei = value; } }

        private string description;
        public string Description { get { return description; } set { description = value; } }

        private string mailingAddress;
        public string MailingAddress { get { return mailingAddress; } set { mailingAddress = value; } }

        private string name;
        public string Name { get { return name; } set { name = value; } }

        private string sector;
        public string Sector { get { return sector; } set { sector = value; } }

        private string shortDescription;
        public string ShortDescription { get { return shortDescription; } set { shortDescription = value; } }

        private string sic;
        public string Sic { get { return sic; } set { sic = value; } }

        private int standarizedActive;
        public int StandarizedActive { get { return standarizedActive; } set { standarizedActive = value; } }

        private string state;
        public string State { get { return state; } set { state = value; } }

        private string stockExchange;
        public string StockExchange { get { return stockExchange; } set { stockExchange = value; } }

        private static DAL.DAL_Company dal = new DAL.DAL_Company();

        public void add()
        {
            this.companyId = dal.Add_Company(
                this.Template,
                this.Ticker,
                this.Active,
                this.BusinessAddress,
                this.BusinessPhoneNbr,
                this.Ceo,
                this.Cik,
                this.CompanyUrl,
                this.Country,
                this.DateCreation,
                (long)this.Employees,
                this.IncCountry,
                this.IncState,
                this.IndustryCategory,
                this.IndustryGroup,
                this.LegalName,
                this.Lei,
                this.Description,
                this.MailingAddress,
                this.Name,
                this.Sector,
                this.ShortDescription,
                this.Sic,
                this.StandarizedActive,
                this.State,
                this.StockExchange);
        }

        public void update()
        {
            dal.Update_Company(
                this.CompanyId,
                this.Template,
                this.Ticker,
                this.Active,
                this.BusinessAddress,
                this.BusinessPhoneNbr,
                this.Ceo,
                this.Cik,
                this.CompanyUrl,
                this.Country,
                this.DateCreation,
                (long)this.Employees,
                this.IncCountry,
                this.IncState,
                this.IndustryCategory,
                this.IndustryGroup,
                this.LegalName,
                this.Lei,
                this.Description,
                this.MailingAddress,
                this.Name,
                this.Sector,
                this.ShortDescription,
                this.Sic,
                this.StandarizedActive,
                this.State,
                this.StockExchange);
        }

        public void delete()
        {
            dal.Delete_Company(this.CompanyId);
        }

        public static void delete(long companyId)
        {
            dal.Delete_Company(companyId);
        }

        public bool getById(long companyId)
        {
            System.Data.DataSet ds = dal.Get_By_Id_Company(companyId);
            return fillStandardizedDataMembers(ref ds);
        }

        private bool fillStandardizedDataMembers(ref System.Data.DataSet aDataSet)
        {
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                if (tempTable.Rows.Count > 0)
                {
                    System.Data.DataRow drow = tempTable.Rows[0];
                    fillObjectFromDataRow(drow, this);
                    return true;
                }
            }
            return false;
        }

        private static List<Company> fillStandardizedData(ref System.Data.DataSet aDataSet)
        {
            List<Company> ret = new List<Company>();
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                for (int i = 0; i < tempTable.Rows.Count; ++i)
                {
                    System.Data.DataRow drow = tempTable.Rows[i];
                    Company temp = new Company();
                    fillObjectFromDataRow(drow, temp);
                    ret.Add(temp);
                }
            }
            return ret;
        }

        private static void fillObjectFromDataRow(System.Data.DataRow drow, Company temp)
        {
            temp.companyId = Convert.ToInt64(drow[DAL.DAL_Company.COMPANYID]);
            temp.Template = Convert.ToString(drow[DAL.DAL_Company.TEMPLATE]);
            temp.Ticker = Convert.ToString(drow[DAL.DAL_Company.TICKER]);
            temp.Active = Convert.ToInt32(drow[DAL.DAL_Company.ACTIVE]);
            temp.BusinessAddress = Convert.ToString(drow[DAL.DAL_Company.BUSINESSADDRESS]);
            temp.BusinessPhoneNbr = Convert.ToString(drow[DAL.DAL_Company.BUSINESSPHONENBR]);
            temp.Ceo = Convert.ToString(drow[DAL.DAL_Company.CEO]);
            temp.Cik = Convert.ToString(drow[DAL.DAL_Company.CIK]);
            temp.CompanyUrl = Convert.ToString(drow[DAL.DAL_Company.COMPANYURL]);
            temp.Country = Convert.ToString(drow[DAL.DAL_Company.COUNTRY]);
            if (string.IsNullOrEmpty(drow[DAL.DAL_Company.DATE_CREATION].ToString()) == false)
            {
                String tempDate = drow[DAL.DAL_Company.DATE_CREATION].ToString().Substring(0, 19);
                temp.DateCreation = DateTime.ParseExact(tempDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                temp.DateCreation = DateTime.Now;
            }
            temp.Employees = Convert.ToInt32(drow[DAL.DAL_Company.EMPLOYEES]);
            temp.IncCountry = Convert.ToString(drow[DAL.DAL_Company.INCCOUNTRY]);
            temp.IncState = Convert.ToString(drow[DAL.DAL_Company.INCSTATE]);
            temp.IndustryCategory = Convert.ToString(drow[DAL.DAL_Company.INDUSTRYCATEGORY]);
            temp.IndustryGroup = Convert.ToString(drow[DAL.DAL_Company.INDUSTRYGROUP]);
            temp.LegalName = Convert.ToString(drow[DAL.DAL_Company.LEGALNAME]);
            temp.Lei = Convert.ToString(drow[DAL.DAL_Company.LEI]);
            temp.Description = Convert.ToString(drow[DAL.DAL_Company.DESCRIPTION]);
            temp.MailingAddress = Convert.ToString(drow[DAL.DAL_Company.MAILINGADDRESS]);
            temp.Name = Convert.ToString(drow[DAL.DAL_Company.NAME]);
            temp.Sector = Convert.ToString(drow[DAL.DAL_Company.SECTOR]);
            temp.ShortDescription = Convert.ToString(drow[DAL.DAL_Company.SHORTDESCRIPTION]);
            temp.Sic = Convert.ToString(drow[DAL.DAL_Company.SIC]);
            temp.StandarizedActive = Convert.ToInt32(drow[DAL.DAL_Company.STANDARIZEDACTIVE]);
            temp.State = Convert.ToString(drow[DAL.DAL_Company.STATE]);
            temp.StockExchange = Convert.ToString(drow[DAL.DAL_Company.STOCKEXCHANGE]);
        }

        public static List<Company> search(string companyId, string template, string ticker, string active, string businessAddress, string businessPhoneNbr, string ceo, string cik, string companyUrl, string country, string date_creationFrom, string date_creationTo, string employees, string incCountry, string incState, string industryCategory, string industryGroup, string legalName, string lei, string Description, string mailingAddress, string name, string sector, string shortDescription, string sic, string standarizedActive, string state, string stockExchange, string sortingField)
        {
            System.Data.DataSet ds = dal.Select_Company(companyId, template, ticker, active, businessAddress, businessPhoneNbr, ceo, cik, companyUrl, country, date_creationFrom, date_creationTo, employees, incCountry, incState, industryCategory, industryGroup, legalName, lei, Description, mailingAddress, name, sector, shortDescription, sic, standarizedActive, state, stockExchange, sortingField);
            return fillStandardizedData(ref ds);
        }
    }
}
