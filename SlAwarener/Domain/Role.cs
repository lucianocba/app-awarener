﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Role
    {
        public static string FIELD_ROLEID = "roleId";
        public static string FIELD_ROLENAME = "roleName";

        private long roleId;
        public long RoleId { get { return roleId; } }

        private string roleName;
        public string RoleName { get { return roleName; } set { roleName = value; } }

        private static DAL.DAL_Role dal = new DAL.DAL_Role();

        public void add()
        {
            this.roleId = dal.Add_Role(
                this.RoleName);
        }

        public void update()
        {
            dal.Update_Role(
                this.RoleId,
                this.RoleName);
        }

        public void delete()
        {
            dal.Delete_Role(this.RoleId);
        }

        public static void delete(long roleId)
        {
            dal.Delete_Role(roleId);
        }

        public bool getById(long roleId)
        {
            System.Data.DataSet ds = dal.Get_By_Id_Role(roleId);
            return fillStandardizedDataMembers(ref ds);
        }

        private bool fillStandardizedDataMembers(ref System.Data.DataSet aDataSet)
        {
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                if (tempTable.Rows.Count > 0)
                {
                    System.Data.DataRow drow = tempTable.Rows[0];
                    fillObjectFromDataRow(drow, this);
                    return true;
                }
            }
            return false;
        }

        private static List<Role> fillStandardizedData(ref System.Data.DataSet aDataSet)
        {
            List<Role> ret = new List<Role>();
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                for (int i = 0; i < tempTable.Rows.Count; ++i)
                {
                    System.Data.DataRow drow = tempTable.Rows[i];
                    Role temp = new Role();
                    fillObjectFromDataRow(drow, temp);
                    ret.Add(temp);
                }
            }
            return ret;
        }

        private static void fillObjectFromDataRow(System.Data.DataRow drow, Role temp)
        {
            temp.roleId = Convert.ToInt64(drow[DAL.DAL_Role.ROLEID]);
            temp.RoleName = Convert.ToString(drow[DAL.DAL_Role.ROLENAME]);
        }

        public static List<Role> search(string roleId, string roleName, string sortingField)
        {
            System.Data.DataSet ds = dal.Select_Role(roleId, roleName, sortingField);
            return fillStandardizedData(ref ds);
        }
    }
}
