﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Access
    {

        public static string FIELD_SUBMENUID = "subMenuId";
        public static string FIELD_ROLEID = "roleId";

        private long subMenuId;
        public long SubMenuId { get { return subMenuId; } set { subMenuId = value; } }

        private long roleId;
        public long RoleId { get { return roleId; } set { roleId = value; } }

        private static DAL.DAL_Access dal = new DAL.DAL_Access();

        public void add()
        {
            dal.Add_Access(
                this.SubMenuId,
                this.RoleId);
        }

        public void update()
        {
            dal.Update_Access(
                this.SubMenuId,
                this.RoleId);
        }

        public bool getById()
        {
            System.Data.DataSet ds = dal.Get_By_Id_Access();
            return fillStandardizedDataMembers(ref ds);
        }

        private bool fillStandardizedDataMembers(ref System.Data.DataSet aDataSet)
        {
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                if (tempTable.Rows.Count > 0)
                {
                    System.Data.DataRow drow = tempTable.Rows[0];
                    fillObjectFromDataRow(drow, this);
                    return true;
                }
            }
            return false;
        }

        private static List<Access> fillStandardizedData(ref System.Data.DataSet aDataSet)
        {
            List<Access> ret = new List<Access>();
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                for (int i = 0; i < tempTable.Rows.Count; ++i)
                {
                    System.Data.DataRow drow = tempTable.Rows[i];
                    Access temp = new Access();
                    fillObjectFromDataRow(drow, temp);
                    ret.Add(temp);
                }
            }
            return ret;
        }

        private static void fillObjectFromDataRow(System.Data.DataRow drow, Access temp)
        {
            temp.SubMenuId = Convert.ToInt64(drow[DAL.DAL_Access.SUBMENUID]);
            temp.RoleId = Convert.ToInt64(drow[DAL.DAL_Access.ROLEID]);
        }

        public static List<Access> search(string subMenuId, string roleId, string sortingField)
        {
            System.Data.DataSet ds = dal.Select_Access(subMenuId, roleId, sortingField);
            return fillStandardizedData(ref ds);
        }
    }
}
