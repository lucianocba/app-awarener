﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Domain
{
    public class Tag
    {
        public static string FIELD_TAGID = "tagId";
        public static string FIELD_NAME = "name";
        public static string FIELD_TAG = "tag";
        public static string FIELD_STATEMENTCODE = "statementCode";
        public static string FIELD_TEMPLATE = "template";
        public static string FIELD_BALANCE = "balance";
        public static string FIELD_TYPE = "type";
        public static string FIELD_UNITS = "units";

        private long tagId;
        public long TagId { get { return tagId; } }

        private string name;
        public string Name { get { return name; } set { name = value; } }

        private string tag;
        public string TagValue { get { return tag; } set { tag = value; } }

        private string statementCode;
        public string StatementCode { get { return statementCode; } set { statementCode = value; } }

        private string template;
        public string Template { get { return template; } set { template = value; } }

        private string balance;
        public string Balance { get { return balance; } set { balance = value; } }

        private string type;
        public string Type { get { return type; } set { type = value; } }

        private string units;
        public string Units { get { return units; } set { units = value; } }

        private static DAL.DAL_Tag dal = new DAL.DAL_Tag();

        public void add()
        {
            this.tagId = dal.Add_Tag(
                this.Name,
                this.TagValue,
                this.StatementCode,
                this.Template,
                this.Balance,
                this.Type,
                this.Units);
        }

        public void update()
        {
            dal.Update_Tag(
                this.TagId,
                this.Name,
                this.TagValue,
                this.StatementCode,
                this.Template,
                this.Balance,
                this.Type,
                this.Units);
        }

        public void delete()
        {
            dal.Delete_Tag(this.TagId);
        }

        public static void delete(long tagId)
        {
            dal.Delete_Tag(tagId);
        }

        public bool getById(long tagId)
        {
            System.Data.DataSet ds = dal.Get_By_Id_Tag(tagId);
            return fillStandardizedDataMembers(ref ds);
        }

        private bool fillStandardizedDataMembers(ref System.Data.DataSet aDataSet)
        {
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                if (tempTable.Rows.Count > 0)
                {
                    System.Data.DataRow drow = tempTable.Rows[0];
                    fillObjectFromDataRow(drow, this);
                    return true;
                }
            }
            return false;
        }

        private static List<Tag> fillStandardizedData(ref System.Data.DataSet aDataSet)
        {
            List<Tag> ret = new List<Tag>();
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                for (int i = 0; i < tempTable.Rows.Count; ++i)
                {
                    System.Data.DataRow drow = tempTable.Rows[i];
                    Tag temp = new Tag();
                    fillObjectFromDataRow(drow, temp);
                    ret.Add(temp);
                }
            }
            return ret;
        }

        private static void fillObjectFromDataRow(System.Data.DataRow drow, Tag temp)
        {
            temp.tagId = Convert.ToInt64(drow[DAL.DAL_Tag.TAGID]);
            temp.Name = Convert.ToString(drow[DAL.DAL_Tag.NAME]);
            temp.TagValue = Convert.ToString(drow[DAL.DAL_Tag.TAG]);
            temp.StatementCode = Convert.ToString(drow[DAL.DAL_Tag.STATEMENTCODE]);
            temp.Template = Convert.ToString(drow[DAL.DAL_Tag.TEMPLATE]);
            temp.Balance = Convert.ToString(drow[DAL.DAL_Tag.BALANCE]);
            temp.Type = Convert.ToString(drow[DAL.DAL_Tag.TYPE]);
            temp.Units = Convert.ToString(drow[DAL.DAL_Tag.UNITS]);
        }

        public static List<Tag> search(string tagId, string name, string tag, string statementCode, string template, string balance, string type, string units, string sortingField)
        {
            System.Data.DataSet ds = dal.Select_Tag(tagId, name, tag, statementCode, template, balance, type, units, sortingField);
            return fillStandardizedData(ref ds);
        }

        public static List<Tag> GetAllTags()
        {

            try
            {
                List<Tag> lstSTTags = new List<Tag>();
                StreamReader file;
                var path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"files\tags.csv");
                file = File.OpenText(path);

                while (!file.EndOfStream)
                {
                    var line = file.ReadLine();
                    var financialtags = line.Split(';');

                    Tag oSTInsTags = new Tag();

                    oSTInsTags.name = financialtags[0];
                    oSTInsTags.tag = financialtags[1];
                    oSTInsTags.statementCode = financialtags[2];
                    oSTInsTags.template = financialtags[3];
                    oSTInsTags.balance = financialtags[4];
                    oSTInsTags.type = financialtags[5];
                    oSTInsTags.units = financialtags[6];

                    lstSTTags.Add(oSTInsTags);
                }

                file.Close();
                return lstSTTags;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


    }
}
