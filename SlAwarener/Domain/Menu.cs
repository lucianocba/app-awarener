﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Menu
    {
        public static string FIELD_MENUID = "menuId";
        public static string FIELD_NAME = "name";
        public static string FIELD_ICON = "icon";

        private long menuId;
        public long MenuId { get { return menuId; } }

        private string name;
        public string Name { get { return name; } set { name = value; } }

        private string icon;
        public string Icon { get { return icon; } set { icon = value; } }

        private static DAL.DAL_Menu dal = new DAL.DAL_Menu();

        public void add()
        {
            this.menuId = dal.Add_Menu(
                this.Name,
                this.Icon);
        }

        public void update()
        {
            dal.Update_Menu(
                this.MenuId,
                this.Name,
                this.Icon);
        }

        public void delete()
        {
            dal.Delete_Menu(this.MenuId);
        }

        public static void delete(long menuId)
        {
            dal.Delete_Menu(menuId);
        }

        public bool getById(long menuId)
        {
            System.Data.DataSet ds = dal.Get_By_Id_Menu(menuId);
            return fillStandardizedDataMembers(ref ds);
        }

        private bool fillStandardizedDataMembers(ref System.Data.DataSet aDataSet)
        {
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                if (tempTable.Rows.Count > 0)
                {
                    System.Data.DataRow drow = tempTable.Rows[0];
                    fillObjectFromDataRow(drow, this);
                    return true;
                }
            }
            return false;
        }

        private static List<Menu> fillStandardizedData(ref System.Data.DataSet aDataSet)
        {
            List<Menu> ret = new List<Menu>();
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                for (int i = 0; i < tempTable.Rows.Count; ++i)
                {
                    System.Data.DataRow drow = tempTable.Rows[i];
                    Menu temp = new Menu();
                    fillObjectFromDataRow(drow, temp);
                    ret.Add(temp);
                }
            }
            return ret;
        }

        private static void fillObjectFromDataRow(System.Data.DataRow drow, Menu temp)
        {
            temp.menuId = Convert.ToInt64(drow[DAL.DAL_Menu.MENUID]);
            temp.Name = Convert.ToString(drow[DAL.DAL_Menu.NAME]);
            temp.Icon = Convert.ToString(drow[DAL.DAL_Menu.ICON]);
        }

        public static List<Menu> search(string menuId, string name, string icon, string sortingField)
        {
            System.Data.DataSet ds = dal.Select_Menu(menuId, name, icon, sortingField);
            return fillStandardizedData(ref ds);
        }
    }
}
