﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class SubMenu
    {
        public static string FIELD_SUBMENUID = "subMenuId";
        public static string FIELD_MENUID = "menuId";
        public static string FIELD_NAME = "name";
        public static string FIELD_CONTROLLER = "controller";
        public static string FIELD_ACTION = "action";

        private long subMenuId;
        public long SubMenuId { get { return subMenuId; } }

        private long menuId;
        public long MenuId { get { return menuId; } set { menuId = value; } }

        private string name;
        public string Name { get { return name; } set { name = value; } }

        private string controller;
        public string Controller { get { return controller; } set { controller = value; } }

        private string action;
        public string Action { get { return action; } set { action = value; } }

        private static DAL.DAL_SubMenu dal = new DAL.DAL_SubMenu();

        public void add()
        {
            this.subMenuId = dal.Add_SubMenu(
                this.MenuId,
                this.Name,
                this.Controller,
                this.Action);
        }

        public void update()
        {
            dal.Update_SubMenu(
                this.SubMenuId,
                this.MenuId,
                this.Name,
                this.Controller,
                this.Action);
        }

        public void delete()
        {
            dal.Delete_SubMenu(this.SubMenuId);
        }

        public static void delete(long subMenuId)
        {
            dal.Delete_SubMenu(subMenuId);
        }

        public bool getById(long subMenuId)
        {
            System.Data.DataSet ds = dal.Get_By_Id_SubMenu(subMenuId);
            return fillStandardizedDataMembers(ref ds);
        }

        private bool fillStandardizedDataMembers(ref System.Data.DataSet aDataSet)
        {
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                if (tempTable.Rows.Count > 0)
                {
                    System.Data.DataRow drow = tempTable.Rows[0];
                    fillObjectFromDataRow(drow, this);
                    return true;
                }
            }
            return false;
        }

        private static List<SubMenu> fillStandardizedData(ref System.Data.DataSet aDataSet)
        {
            List<SubMenu> ret = new List<SubMenu>();
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                for (int i = 0; i < tempTable.Rows.Count; ++i)
                {
                    System.Data.DataRow drow = tempTable.Rows[i];
                    SubMenu temp = new SubMenu();
                    fillObjectFromDataRow(drow, temp);
                    ret.Add(temp);
                }
            }
            return ret;
        }

        private static void fillObjectFromDataRow(System.Data.DataRow drow, SubMenu temp)
        {
            temp.subMenuId = Convert.ToInt64(drow[DAL.DAL_SubMenu.SUBMENUID]);
            temp.MenuId = Convert.ToInt64(drow[DAL.DAL_SubMenu.MENUID]);
            temp.Name = Convert.ToString(drow[DAL.DAL_SubMenu.NAME]);
            temp.Controller = Convert.ToString(drow[DAL.DAL_SubMenu.CONTROLLER]);
            temp.Action = Convert.ToString(drow[DAL.DAL_SubMenu.ACTION]);
        }

        public static List<SubMenu> search(string subMenuId, string menuId, string name, string controller, string action, string sortingField)
        {
            System.Data.DataSet ds = dal.Select_SubMenu(subMenuId, menuId, name, controller, action, sortingField);
            return fillStandardizedData(ref ds);
        }
    }
}
