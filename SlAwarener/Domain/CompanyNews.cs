using System;
using System.Collections.Generic;

namespace Domain
{

    public class CompanyNews
    {
        public static string FIELD_COMPANYNEWSID = "companyNewsId";
        public static string FIELD_COMPANYID = "companyId";
        public static string FIELD_ACTIVE = "active";
        public static string FIELD_DATE_CREATION = "date_creation";
        public static string FIELD_DATE_PUBLICATIONDATE = "date_publicationDate";
        public static string FIELD_SUMMARY = "summary";
        public static string FIELD_TITLE = "title";
        public static string FIELD_DATE_UPDATED = "date_updated";
        public static string FIELD_URL = "url";

        private long companyNewsId;
        public long CompanyNewsId { get { return companyNewsId; } }

        private long companyId;
        public long CompanyId { get { return companyId; } set { companyId = value; } }

        private int active;
        public int Active { get { return active; } set { active = value; } }

        private DateTime dateCreation;
        public DateTime DateCreation { get { return dateCreation; } set { dateCreation = value; } }

        private DateTime datePublicationDate;
        public DateTime DatePublicationDate { get { return datePublicationDate; } set { datePublicationDate = value; } }

        private string summary;
        public string Summary { get { return summary; } set { summary = value; } }

        private string title;
        public string Title { get { return title; } set { title = value; } }

        private DateTime dateUpdated;
        public DateTime DateUpdated { get { return dateUpdated; } set { dateUpdated = value; } }

        private string url;
        public string Url { get { return url; } set { url = value; } }

        private static DAL.DAL_CompanyNews dal = new DAL.DAL_CompanyNews();

        public void add()
        {
            this.companyNewsId = dal.Add_CompanyNews(
                this.CompanyId,
                this.Active,
                this.DateCreation,
                this.DatePublicationDate,
                this.Summary,
                this.Title,
                this.DateUpdated,
                this.Url);
        }

        public void update()
        {
            dal.Update_CompanyNews(
                this.CompanyNewsId,
                this.CompanyId,
                this.Active,
                this.DateCreation,
                this.DatePublicationDate,
                this.Summary,
                this.Title,
                this.DateUpdated,
                this.Url);
        }

        public void delete()
        {
            dal.Delete_CompanyNews(this.CompanyNewsId);
        }

        public static void delete(long companyNewsId)
        {
            dal.Delete_CompanyNews(companyNewsId);
        }

        public bool getById(long companyNewsId)
        {
            System.Data.DataSet ds = dal.Get_By_Id_CompanyNews(companyNewsId);
            return fillStandardizedDataMembers(ref ds);
        }

        private bool fillStandardizedDataMembers(ref System.Data.DataSet aDataSet)
        {
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                if (tempTable.Rows.Count > 0)
                {
                    System.Data.DataRow drow = tempTable.Rows[0];
                    fillObjectFromDataRow(drow, this);
                    return true;
                }
            }
            return false;
        }

        private static List<CompanyNews> fillStandardizedData(ref System.Data.DataSet aDataSet)
        {
            List<CompanyNews> ret = new List<CompanyNews>();
            if (aDataSet.Tables.Count > 0)
            {
                System.Data.DataTable tempTable = aDataSet.Tables[0];
                for (int i = 0; i < tempTable.Rows.Count; ++i)
                {
                    System.Data.DataRow drow = tempTable.Rows[i];
                    CompanyNews temp = new CompanyNews();
                    fillObjectFromDataRow(drow, temp);
                    ret.Add(temp);
                }
            }
            return ret;
        }

        private static void fillObjectFromDataRow(System.Data.DataRow drow, CompanyNews temp)
        {
            temp.companyNewsId = Convert.ToInt64(drow[DAL.DAL_CompanyNews.COMPANYNEWSID]);
            temp.CompanyId = Convert.ToInt64(drow[DAL.DAL_CompanyNews.COMPANYID]);
            temp.Active = Convert.ToInt32(drow[DAL.DAL_CompanyNews.ACTIVE]);
            if (string.IsNullOrEmpty(drow[DAL.DAL_CompanyNews.DATE_CREATION].ToString()) == false)
            {
                String tempDate = drow[DAL.DAL_CompanyNews.DATE_CREATION].ToString().Substring(0, 19);
                temp.DateCreation = DateTime.ParseExact(tempDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                temp.DateCreation = DateTime.Now;
            }
            if (string.IsNullOrEmpty(drow[DAL.DAL_CompanyNews.DATE_PUBLICATIONDATE].ToString()) == false)
            {
                String tempDate = drow[DAL.DAL_CompanyNews.DATE_PUBLICATIONDATE].ToString().Substring(0, 19);
                temp.DatePublicationDate = DateTime.ParseExact(tempDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                temp.DatePublicationDate = DateTime.Now;
            }
            temp.Summary = Convert.ToString(drow[DAL.DAL_CompanyNews.SUMMARY]);
            temp.Title = Convert.ToString(drow[DAL.DAL_CompanyNews.TITLE]);
            if (string.IsNullOrEmpty(drow[DAL.DAL_CompanyNews.DATE_UPDATED].ToString()) == false)
            {
                String tempDate = drow[DAL.DAL_CompanyNews.DATE_UPDATED].ToString().Substring(0, 19);
                temp.DateUpdated = DateTime.ParseExact(tempDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                temp.DateUpdated = DateTime.Now;
            }
            temp.Url = Convert.ToString(drow[DAL.DAL_CompanyNews.URL]);
        }

        public static List<CompanyNews> search(string companyNewsId, string companyId, string active, string date_creationFrom, string date_creationTo, string date_publicationDateFrom, string date_publicationDateTo, string summary, string title, string date_updatedFrom, string date_updatedTo, string url, string sortingField)
        {
            System.Data.DataSet ds = dal.Select_CompanyNews(companyNewsId, companyId, active, date_creationFrom, date_creationTo, date_publicationDateFrom, date_publicationDateTo, summary, title, date_updatedFrom, date_updatedTo, url, sortingField);
            return fillStandardizedData(ref ds);
        }
    }
}
