using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace DAL
{

    public class DAL_Tag
    {

        public static string TAGID = "tagId";
        public static string NAME = "name";
        public static string TAG = "tag";
        public static string STATEMENTCODE = "statementCode";
        public static string TEMPLATE = "template";
        public static string BALANCE = "balance";
        public static string TYPE = "type";
        public static string UNITS = "units";

        public long Add_Tag(string  name, string  tag, string  statementCode, string  template, string  balance, string  type, string  units)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[7];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@name", name);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@tag", tag);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@statementCode", statementCode);
            idbParam[3] = new System.Data.SQLite.SQLiteParameter("@template", template);
            idbParam[4] = new System.Data.SQLite.SQLiteParameter("@balance", balance);
            idbParam[5] = new System.Data.SQLite.SQLiteParameter("@type", type);
            idbParam[6] = new System.Data.SQLite.SQLiteParameter("@units", units);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long id = db.ExecSentenceSQL("INSERT INTO Tag(name, tag, statementCode, template, balance, type, units) VALUES (@name, @tag, @statementCode, @template, @balance, @type, @units)", idbParam, true);
            return id;
        }
        
        public long Update_Tag(long  tagId, string  name, string  tag, string  statementCode, string  template, string  balance, string  type, string  units)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[8];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@tagId", tagId);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@name", name);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@tag", tag);
            idbParam[3] = new System.Data.SQLite.SQLiteParameter("@statementCode", statementCode);
            idbParam[4] = new System.Data.SQLite.SQLiteParameter("@template", template);
            idbParam[5] = new System.Data.SQLite.SQLiteParameter("@balance", balance);
            idbParam[6] = new System.Data.SQLite.SQLiteParameter("@type", type);
            idbParam[7] = new System.Data.SQLite.SQLiteParameter("@units", units);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("UPDATE Tag SET name = @name, tag = @tag, statementCode = @statementCode, template = @template, balance = @balance, type = @type, units = @units WHERE tagId = @tagId", idbParam);
        
            return rowsAffected;
        }
        
        public long Delete_Tag(long  tagId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@tagId", tagId);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("DELETE FROM Tag WHERE tagId = @tagId", idbParam);
        
            return rowsAffected;
        }
        
        public DataSet Select_Tag(string tagId, string name, string tag, string statementCode, string template, string balance, string type, string units, string sortingField)
        {
            int count = 0;
            if (String.IsNullOrEmpty(tagId) == false) ++count;
            if (String.IsNullOrEmpty(name) == false) ++count;
            if (String.IsNullOrEmpty(tag) == false) ++count;
            if (String.IsNullOrEmpty(statementCode) == false) ++count;
            if (String.IsNullOrEmpty(template) == false) ++count;
            if (String.IsNullOrEmpty(balance) == false) ++count;
            if (String.IsNullOrEmpty(type) == false) ++count;
            if (String.IsNullOrEmpty(units) == false) ++count;
            ++count; // sorting field
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[count];
        
            int index = 0;
            if (String.IsNullOrEmpty(tagId) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@tagId", tagId);
                ++index;
            }
            if (String.IsNullOrEmpty(name) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@name", name);
                ++index;
            }
            if (String.IsNullOrEmpty(tag) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@tag", tag);
                ++index;
            }
            if (String.IsNullOrEmpty(statementCode) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@statementCode", statementCode);
                ++index;
            }
            if (String.IsNullOrEmpty(template) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@template", template);
                ++index;
            }
            if (String.IsNullOrEmpty(balance) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@balance", balance);
                ++index;
            }
            if (String.IsNullOrEmpty(type) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@type", type);
                ++index;
            }
            if (String.IsNullOrEmpty(units) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@units", units);
                ++index;
            }
            idbParam[index] = new System.Data.SQLite.SQLiteParameter("@sortingField", sortingField);
            string strSQLSentence = "SELECT * FROM Tag WHERE ";
            bool firstLine = true;
            if (String.IsNullOrEmpty(tagId) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "tagId = @tagId";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND tagId = @tagId";
                }
            }
            if (String.IsNullOrEmpty(name) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "name = @name";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND name = @name";
                }
            }
            if (String.IsNullOrEmpty(tag) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "tag = @tag";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND tag = @tag";
                }
            }
            if (String.IsNullOrEmpty(statementCode) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "statementCode = @statementCode";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND statementCode = @statementCode";
                }
            }
            if (String.IsNullOrEmpty(template) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "template = @template";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND template = @template";
                }
            }
            if (String.IsNullOrEmpty(balance) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "balance = @balance";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND balance = @balance";
                }
            }
            if (String.IsNullOrEmpty(type) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "type = @type";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND type = @type";
                }
            }
            if (String.IsNullOrEmpty(units) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "units = @units";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND units = @units";
                }
            }
            strSQLSentence += " ORDER BY @sortingField";
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS(strSQLSentence, "Tag", idbParam, ref ret);
        }
        
        public DataSet Get_By_Id_Tag(long  tagId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@tagId", tagId);
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS("SELECT * FROM Tag WHERE tagId = @tagId", "Tag", idbParam, ref ret);
        }
    }
}
