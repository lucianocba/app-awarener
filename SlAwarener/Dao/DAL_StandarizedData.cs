using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace DAL
{

    public class DAL_StandarizedData
    {

        public static string STANDARIZEDDATAID = "standarizedDataId";
        public static string ACTIVE = "active";
        public static string DATE_CREATION = "date_creation";
        public static string FISCALPERIOD = "fiscalPeriod";
        public static string FISCALYEAR = "fiscalYear";
        public static string IDCOMPANY = "idCompany";
        public static string STATEMENT = "statement";
        public static string TAG = "tag";
        public static string DATE_UPDATED = "date_updated";
        public static string VALUE = "value";

        public long Add_StandarizedData(long  active, DateTime  date_creation, string  fiscalPeriod, long  fiscalYear, long  idCompany, string  statement, string  tag, DateTime  date_updated, decimal  value)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[9];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@active", active);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@date_creation", date_creation);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@fiscalPeriod", fiscalPeriod);
            idbParam[3] = new System.Data.SQLite.SQLiteParameter("@fiscalYear", fiscalYear);
            idbParam[4] = new System.Data.SQLite.SQLiteParameter("@idCompany", idCompany);
            idbParam[5] = new System.Data.SQLite.SQLiteParameter("@statement", statement);
            idbParam[6] = new System.Data.SQLite.SQLiteParameter("@tag", tag);
            idbParam[7] = new System.Data.SQLite.SQLiteParameter("@date_updated", date_updated);
            idbParam[8] = new System.Data.SQLite.SQLiteParameter("@value", value);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long id = db.ExecSentenceSQL("INSERT INTO StandarizedData(active, date_creation, fiscalPeriod, fiscalYear, idCompany, statement, tag, date_updated, value) VALUES (@active, @date_creation, @fiscalPeriod, @fiscalYear, @idCompany, @statement, @tag, @date_updated, @value)", idbParam, true);
            return id;
        }
        
        public long Update_StandarizedData(long  standarizedDataId, long  active, DateTime  date_creation, string  fiscalPeriod, long  fiscalYear, long  idCompany, string  statement, string  tag, DateTime  date_updated, decimal  value)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[10];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@standarizedDataId", standarizedDataId);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@active", active);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@date_creation", date_creation);
            idbParam[3] = new System.Data.SQLite.SQLiteParameter("@fiscalPeriod", fiscalPeriod);
            idbParam[4] = new System.Data.SQLite.SQLiteParameter("@fiscalYear", fiscalYear);
            idbParam[5] = new System.Data.SQLite.SQLiteParameter("@idCompany", idCompany);
            idbParam[6] = new System.Data.SQLite.SQLiteParameter("@statement", statement);
            idbParam[7] = new System.Data.SQLite.SQLiteParameter("@tag", tag);
            idbParam[8] = new System.Data.SQLite.SQLiteParameter("@date_updated", date_updated);
            idbParam[9] = new System.Data.SQLite.SQLiteParameter("@value", value);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("UPDATE StandarizedData SET active = @active, date_creation = @date_creation, fiscalPeriod = @fiscalPeriod, fiscalYear = @fiscalYear, idCompany = @idCompany, statement = @statement, tag = @tag, date_updated = @date_updated, value = @value WHERE standarizedDataId = @standarizedDataId", idbParam);
        
            return rowsAffected;
        }
        
        public long Delete_StandarizedData(long  standarizedDataId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@standarizedDataId", standarizedDataId);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("DELETE FROM StandarizedData WHERE standarizedDataId = @standarizedDataId", idbParam);
        
            return rowsAffected;
        }
        
        public DataSet Select_StandarizedData(string standarizedDataId, string active, string date_creationFrom, string date_creationTo, string fiscalPeriod, string fiscalYear, string idCompany, string statement, string tag, string date_updatedFrom, string date_updatedTo, string value, string sortingField)
        {
            int count = 0;
            if (String.IsNullOrEmpty(standarizedDataId) == false) ++count;
            if (String.IsNullOrEmpty(active) == false) ++count;
            if (String.IsNullOrEmpty(date_creationFrom) == false) ++count;
            if (String.IsNullOrEmpty(date_creationTo) == false) ++count;
            if (String.IsNullOrEmpty(fiscalPeriod) == false) ++count;
            if (String.IsNullOrEmpty(fiscalYear) == false) ++count;
            if (String.IsNullOrEmpty(idCompany) == false) ++count;
            if (String.IsNullOrEmpty(statement) == false) ++count;
            if (String.IsNullOrEmpty(tag) == false) ++count;
            if (String.IsNullOrEmpty(date_updatedFrom) == false) ++count;
            if (String.IsNullOrEmpty(date_updatedTo) == false) ++count;
            if (String.IsNullOrEmpty(value) == false) ++count;
            ++count; // sorting field
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[count];
        
            int index = 0;
            if (String.IsNullOrEmpty(standarizedDataId) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@standarizedDataId", standarizedDataId);
                ++index;
            }
            if (String.IsNullOrEmpty(active) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@active", active);
                ++index;
            }
            if (String.IsNullOrEmpty(date_creationFrom) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_creationFrom", date_creationFrom);
                ++index;
            }
            if (String.IsNullOrEmpty(date_creationTo) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_creationTo", date_creationTo);
                ++index;
            }
            if (String.IsNullOrEmpty(fiscalPeriod) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@fiscalPeriod", fiscalPeriod);
                ++index;
            }
            if (String.IsNullOrEmpty(fiscalYear) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@fiscalYear", fiscalYear);
                ++index;
            }
            if (String.IsNullOrEmpty(idCompany) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@idCompany", idCompany);
                ++index;
            }
            if (String.IsNullOrEmpty(statement) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@statement", statement);
                ++index;
            }
            if (String.IsNullOrEmpty(tag) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@tag", tag);
                ++index;
            }
            if (String.IsNullOrEmpty(date_updatedFrom) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_updatedFrom", date_updatedFrom);
                ++index;
            }
            if (String.IsNullOrEmpty(date_updatedTo) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_updatedTo", date_updatedTo);
                ++index;
            }
            if (String.IsNullOrEmpty(value) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@value", value);
                ++index;
            }
            idbParam[index] = new System.Data.SQLite.SQLiteParameter("@sortingField", sortingField);
            string strSQLSentence = "SELECT * FROM StandarizedData WHERE ";
            bool firstLine = true;
            if (String.IsNullOrEmpty(standarizedDataId) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "standarizedDataId = @standarizedDataId";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND standarizedDataId = @standarizedDataId";
                }
            }
            if (String.IsNullOrEmpty(active) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "active = @active";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND active = @active";
                }
            }
            if (String.IsNullOrEmpty(date_creationFrom) == false && String.IsNullOrEmpty(date_creationTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_creation BETWEEN @date_creationFrom AND @date_creationTo";
                    firstLine = false;
                }
                else
                {
                   strSQLSentence += " AND date_creation BETWEEN @date_creationFrom AND @date_creationTo";
                }
            }
            else if (String.IsNullOrEmpty(date_creationFrom) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_creation >= @date_creationFrom";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_creation >= @date_creationFrom";
                }
            }
            else if (String.IsNullOrEmpty(date_creationTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_creation <= @date_creationTo";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_creation <= @date_creationTo";
                }
            }
            if (String.IsNullOrEmpty(fiscalPeriod) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "fiscalPeriod = @fiscalPeriod";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND fiscalPeriod = @fiscalPeriod";
                }
            }
            if (String.IsNullOrEmpty(fiscalYear) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "fiscalYear = @fiscalYear";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND fiscalYear = @fiscalYear";
                }
            }
            if (String.IsNullOrEmpty(idCompany) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "idCompany = @idCompany";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND idCompany = @idCompany";
                }
            }
            if (String.IsNullOrEmpty(statement) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "statement = @statement";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND statement = @statement";
                }
            }
            if (String.IsNullOrEmpty(tag) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "tag = @tag";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND tag = @tag";
                }
            }
            if (String.IsNullOrEmpty(date_updatedFrom) == false && String.IsNullOrEmpty(date_updatedTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_updated BETWEEN @date_updatedFrom AND @date_updatedTo";
                    firstLine = false;
                }
                else
                {
                   strSQLSentence += " AND date_updated BETWEEN @date_updatedFrom AND @date_updatedTo";
                }
            }
            else if (String.IsNullOrEmpty(date_updatedFrom) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_updated >= @date_updatedFrom";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_updated >= @date_updatedFrom";
                }
            }
            else if (String.IsNullOrEmpty(date_updatedTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_updated <= @date_updatedTo";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_updated <= @date_updatedTo";
                }
            }
            if (String.IsNullOrEmpty(value) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "value = @value";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND value = @value";
                }
            }
            strSQLSentence += " ORDER BY @sortingField";
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS(strSQLSentence, "StandarizedData", idbParam, ref ret);
        }
        
        public DataSet Get_By_Id_StandarizedData(long  standarizedDataId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@standarizedDataId", standarizedDataId);
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS("SELECT * FROM StandarizedData WHERE standarizedDataId = @standarizedDataId", "StandarizedData", idbParam, ref ret);
        }
    }
}
