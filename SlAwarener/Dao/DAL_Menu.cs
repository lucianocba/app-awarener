using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace DAL
{

    public class DAL_Menu
    {

        public static string MENUID = "menuId";
        public static string NAME = "name";
        public static string ICON = "icon";

        public long Add_Menu(string  name, string  icon)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[2];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@name", name);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@icon", icon);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long id = db.ExecSentenceSQL("INSERT INTO Menu(name, icon) VALUES (@name, @icon)", idbParam, true);
            return id;
        }
        
        public long Update_Menu(long  menuId, string  name, string  icon)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[3];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@menuId", menuId);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@name", name);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@icon", icon);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("UPDATE Menu SET name = @name, icon = @icon WHERE menuId = @menuId", idbParam);
        
            return rowsAffected;
        }
        
        public long Delete_Menu(long  menuId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@menuId", menuId);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("DELETE FROM Menu WHERE menuId = @menuId", idbParam);
        
            return rowsAffected;
        }
        
        public DataSet Select_Menu(string menuId, string name, string icon, string sortingField)
        {
            int count = 0;
            if (String.IsNullOrEmpty(menuId) == false) ++count;
            if (String.IsNullOrEmpty(name) == false) ++count;
            if (String.IsNullOrEmpty(icon) == false) ++count;
            ++count; // sorting field
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[count];
        
            int index = 0;
            if (String.IsNullOrEmpty(menuId) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@menuId", menuId);
                ++index;
            }
            if (String.IsNullOrEmpty(name) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@name", name);
                ++index;
            }
            if (String.IsNullOrEmpty(icon) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@icon", icon);
                ++index;
            }
            idbParam[index] = new System.Data.SQLite.SQLiteParameter("@sortingField", sortingField);
            string strSQLSentence = "SELECT * FROM Menu WHERE ";
            bool firstLine = true;
            if (String.IsNullOrEmpty(menuId) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "menuId = @menuId";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND menuId = @menuId";
                }
            }
            if (String.IsNullOrEmpty(name) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "name = @name";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND name = @name";
                }
            }
            if (String.IsNullOrEmpty(icon) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "icon = @icon";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND icon = @icon";
                }
            }
            strSQLSentence += " ORDER BY @sortingField";
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS(strSQLSentence, "Menu", idbParam, ref ret);
        }
        
        public DataSet Get_By_Id_Menu(long  menuId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@menuId", menuId);
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS("SELECT * FROM Menu WHERE menuId = @menuId", "Menu", idbParam, ref ret);
        }
    }
}
