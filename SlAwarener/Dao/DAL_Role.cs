using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace DAL
{

    public class DAL_Role
    {

        public static string ROLEID = "roleId";
        public static string ROLENAME = "roleName";

        public long Add_Role(string  roleName)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@roleName", roleName);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long id = db.ExecSentenceSQL("INSERT INTO Role(roleName) VALUES (@roleName)", idbParam, true);
            return id;
        }
        
        public long Update_Role(long  roleId, string  roleName)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[2];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@roleId", roleId);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@roleName", roleName);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("UPDATE Role SET roleName = @roleName WHERE roleId = @roleId", idbParam);
        
            return rowsAffected;
        }
        
        public long Delete_Role(long  roleId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@roleId", roleId);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("DELETE FROM Role WHERE roleId = @roleId", idbParam);
        
            return rowsAffected;
        }
        
        public DataSet Select_Role(string roleId, string roleName, string sortingField)
        {
            int count = 0;
            if (String.IsNullOrEmpty(roleId) == false) ++count;
            if (String.IsNullOrEmpty(roleName) == false) ++count;
            ++count; // sorting field
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[count];
        
            int index = 0;
            if (String.IsNullOrEmpty(roleId) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@roleId", roleId);
                ++index;
            }
            if (String.IsNullOrEmpty(roleName) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@roleName", roleName);
                ++index;
            }
            idbParam[index] = new System.Data.SQLite.SQLiteParameter("@sortingField", sortingField);
            string strSQLSentence = "SELECT * FROM Role WHERE ";
            bool firstLine = true;
            if (String.IsNullOrEmpty(roleId) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "roleId = @roleId";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND roleId = @roleId";
                }
            }
            if (String.IsNullOrEmpty(roleName) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "roleName = @roleName";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND roleName = @roleName";
                }
            }
            strSQLSentence += " ORDER BY @sortingField";
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS(strSQLSentence, "Role", idbParam, ref ret);
        }
        
        public DataSet Get_By_Id_Role(long  roleId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@roleId", roleId);
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS("SELECT * FROM Role WHERE roleId = @roleId", "Role", idbParam, ref ret);
        }
    }
}
