using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace DAL
{

    public class DAL_SubMenu
    {

        public static string SUBMENUID = "subMenuId";
        public static string MENUID = "menuId";
        public static string NAME = "name";
        public static string CONTROLLER = "controller";
        public static string ACTION = "action";

        public long Add_SubMenu(long  menuId, string  name, string  controller, string  action)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[4];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@menuId", menuId);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@name", name);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@controller", controller);
            idbParam[3] = new System.Data.SQLite.SQLiteParameter("@action", action);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long id = db.ExecSentenceSQL("INSERT INTO SubMenu(menuId, name, controller, action) VALUES (@menuId, @name, @controller, @action)", idbParam, true);
            return id;
        }
        
        public long Update_SubMenu(long  subMenuId, long  menuId, string  name, string  controller, string  action)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[5];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@subMenuId", subMenuId);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@menuId", menuId);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@name", name);
            idbParam[3] = new System.Data.SQLite.SQLiteParameter("@controller", controller);
            idbParam[4] = new System.Data.SQLite.SQLiteParameter("@action", action);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("UPDATE SubMenu SET menuId = @menuId, name = @name, controller = @controller, action = @action WHERE subMenuId = @subMenuId", idbParam);
        
            return rowsAffected;
        }
        
        public long Delete_SubMenu(long  subMenuId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@subMenuId", subMenuId);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("DELETE FROM SubMenu WHERE subMenuId = @subMenuId", idbParam);
        
            return rowsAffected;
        }
        
        public DataSet Select_SubMenu(string subMenuId, string menuId, string name, string controller, string action, string sortingField)
        {
            int count = 0;
            if (String.IsNullOrEmpty(subMenuId) == false) ++count;
            if (String.IsNullOrEmpty(menuId) == false) ++count;
            if (String.IsNullOrEmpty(name) == false) ++count;
            if (String.IsNullOrEmpty(controller) == false) ++count;
            if (String.IsNullOrEmpty(action) == false) ++count;
            ++count; // sorting field
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[count];
        
            int index = 0;
            if (String.IsNullOrEmpty(subMenuId) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@subMenuId", subMenuId);
                ++index;
            }
            if (String.IsNullOrEmpty(menuId) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@menuId", menuId);
                ++index;
            }
            if (String.IsNullOrEmpty(name) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@name", name);
                ++index;
            }
            if (String.IsNullOrEmpty(controller) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@controller", controller);
                ++index;
            }
            if (String.IsNullOrEmpty(action) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@action", action);
                ++index;
            }
            idbParam[index] = new System.Data.SQLite.SQLiteParameter("@sortingField", sortingField);
            string strSQLSentence = "SELECT * FROM SubMenu WHERE ";
            bool firstLine = true;
            if (String.IsNullOrEmpty(subMenuId) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "subMenuId = @subMenuId";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND subMenuId = @subMenuId";
                }
            }
            if (String.IsNullOrEmpty(menuId) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "menuId = @menuId";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND menuId = @menuId";
                }
            }
            if (String.IsNullOrEmpty(name) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "name = @name";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND name = @name";
                }
            }
            if (String.IsNullOrEmpty(controller) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "controller = @controller";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND controller = @controller";
                }
            }
            if (String.IsNullOrEmpty(action) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "action = @action";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND action = @action";
                }
            }
            strSQLSentence += " ORDER BY @sortingField";
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS(strSQLSentence, "SubMenu", idbParam, ref ret);
        }
        
        public DataSet Get_By_Id_SubMenu(long  subMenuId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@subMenuId", subMenuId);
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS("SELECT * FROM SubMenu WHERE subMenuId = @subMenuId", "SubMenu", idbParam, ref ret);
        }
    }
}
