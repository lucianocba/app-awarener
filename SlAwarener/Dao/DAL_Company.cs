using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace DAL
{

    public class DAL_Company
    {

        public static string COMPANYID = "companyId";
        public static string TEMPLATE = "template";
        public static string TICKER = "ticker";
        public static string ACTIVE = "active";
        public static string BUSINESSADDRESS = "businessAddress";
        public static string BUSINESSPHONENBR = "businessPhoneNbr";
        public static string CEO = "ceo";
        public static string CIK = "cik";
        public static string COMPANYURL = "companyUrl";
        public static string COUNTRY = "country";
        public static string DATE_CREATION = "date_creation";
        public static string EMPLOYEES = "employees";
        public static string INCCOUNTRY = "incCountry";
        public static string INCSTATE = "incState";
        public static string INDUSTRYCATEGORY = "industryCategory";
        public static string INDUSTRYGROUP = "industryGroup";
        public static string LEGALNAME = "legalName";
        public static string LEI = "lei";
        public static string DESCRIPTION = "Description";
        public static string MAILINGADDRESS = "mailingAddress";
        public static string NAME = "name";
        public static string SECTOR = "sector";
        public static string SHORTDESCRIPTION = "shortDescription";
        public static string SIC = "sic";
        public static string STANDARIZEDACTIVE = "standarizedActive";
        public static string STATE = "state";
        public static string STOCKEXCHANGE = "stockExchange";

        public long Add_Company(string  template, string  ticker, long  active, string  businessAddress, string  businessPhoneNbr, string  ceo, string  cik, string  companyUrl, string  country, DateTime  date_creation, long  employees, string  incCountry, string  incState, string  industryCategory, string  industryGroup, string  legalName, string  lei, string  Description, string  mailingAddress, string  name, string  sector, string  shortDescription, string  sic, long  standarizedActive, string  state, string  stockExchange)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[26];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@template", template);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@ticker", ticker);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@active", active);
            idbParam[3] = new System.Data.SQLite.SQLiteParameter("@businessAddress", businessAddress);
            idbParam[4] = new System.Data.SQLite.SQLiteParameter("@businessPhoneNbr", businessPhoneNbr);
            idbParam[5] = new System.Data.SQLite.SQLiteParameter("@ceo", ceo);
            idbParam[6] = new System.Data.SQLite.SQLiteParameter("@cik", cik);
            idbParam[7] = new System.Data.SQLite.SQLiteParameter("@companyUrl", companyUrl);
            idbParam[8] = new System.Data.SQLite.SQLiteParameter("@country", country);
            idbParam[9] = new System.Data.SQLite.SQLiteParameter("@date_creation", date_creation);
            idbParam[10] = new System.Data.SQLite.SQLiteParameter("@employees", employees);
            idbParam[11] = new System.Data.SQLite.SQLiteParameter("@incCountry", incCountry);
            idbParam[12] = new System.Data.SQLite.SQLiteParameter("@incState", incState);
            idbParam[13] = new System.Data.SQLite.SQLiteParameter("@industryCategory", industryCategory);
            idbParam[14] = new System.Data.SQLite.SQLiteParameter("@industryGroup", industryGroup);
            idbParam[15] = new System.Data.SQLite.SQLiteParameter("@legalName", legalName);
            idbParam[16] = new System.Data.SQLite.SQLiteParameter("@lei", lei);
            idbParam[17] = new System.Data.SQLite.SQLiteParameter("@Description", Description);
            idbParam[18] = new System.Data.SQLite.SQLiteParameter("@mailingAddress", mailingAddress);
            idbParam[19] = new System.Data.SQLite.SQLiteParameter("@name", name);
            idbParam[20] = new System.Data.SQLite.SQLiteParameter("@sector", sector);
            idbParam[21] = new System.Data.SQLite.SQLiteParameter("@shortDescription", shortDescription);
            idbParam[22] = new System.Data.SQLite.SQLiteParameter("@sic", sic);
            idbParam[23] = new System.Data.SQLite.SQLiteParameter("@standarizedActive", standarizedActive);
            idbParam[24] = new System.Data.SQLite.SQLiteParameter("@state", state);
            idbParam[25] = new System.Data.SQLite.SQLiteParameter("@stockExchange", stockExchange);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long id = db.ExecSentenceSQL("INSERT INTO Company(template, ticker, active, businessAddress, businessPhoneNbr, ceo, cik, companyUrl, country, date_creation, employees, incCountry, incState, industryCategory, industryGroup, legalName, lei, Description, mailingAddress, name, sector, shortDescription, sic, standarizedActive, state, stockExchange) VALUES (@template, @ticker, @active, @businessAddress, @businessPhoneNbr, @ceo, @cik, @companyUrl, @country, @date_creation, @employees, @incCountry, @incState, @industryCategory, @industryGroup, @legalName, @lei, @Description, @mailingAddress, @name, @sector, @shortDescription, @sic, @standarizedActive, @state, @stockExchange)", idbParam, true);
            return id;
        }
        
        public long Update_Company(long  companyId, string  template, string  ticker, long  active, string  businessAddress, string  businessPhoneNbr, string  ceo, string  cik, string  companyUrl, string  country, DateTime  date_creation, long  employees, string  incCountry, string  incState, string  industryCategory, string  industryGroup, string  legalName, string  lei, string  Description, string  mailingAddress, string  name, string  sector, string  shortDescription, string  sic, long  standarizedActive, string  state, string  stockExchange)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[27];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@companyId", companyId);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@template", template);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@ticker", ticker);
            idbParam[3] = new System.Data.SQLite.SQLiteParameter("@active", active);
            idbParam[4] = new System.Data.SQLite.SQLiteParameter("@businessAddress", businessAddress);
            idbParam[5] = new System.Data.SQLite.SQLiteParameter("@businessPhoneNbr", businessPhoneNbr);
            idbParam[6] = new System.Data.SQLite.SQLiteParameter("@ceo", ceo);
            idbParam[7] = new System.Data.SQLite.SQLiteParameter("@cik", cik);
            idbParam[8] = new System.Data.SQLite.SQLiteParameter("@companyUrl", companyUrl);
            idbParam[9] = new System.Data.SQLite.SQLiteParameter("@country", country);
            idbParam[10] = new System.Data.SQLite.SQLiteParameter("@date_creation", date_creation);
            idbParam[11] = new System.Data.SQLite.SQLiteParameter("@employees", employees);
            idbParam[12] = new System.Data.SQLite.SQLiteParameter("@incCountry", incCountry);
            idbParam[13] = new System.Data.SQLite.SQLiteParameter("@incState", incState);
            idbParam[14] = new System.Data.SQLite.SQLiteParameter("@industryCategory", industryCategory);
            idbParam[15] = new System.Data.SQLite.SQLiteParameter("@industryGroup", industryGroup);
            idbParam[16] = new System.Data.SQLite.SQLiteParameter("@legalName", legalName);
            idbParam[17] = new System.Data.SQLite.SQLiteParameter("@lei", lei);
            idbParam[18] = new System.Data.SQLite.SQLiteParameter("@Description", Description);
            idbParam[19] = new System.Data.SQLite.SQLiteParameter("@mailingAddress", mailingAddress);
            idbParam[20] = new System.Data.SQLite.SQLiteParameter("@name", name);
            idbParam[21] = new System.Data.SQLite.SQLiteParameter("@sector", sector);
            idbParam[22] = new System.Data.SQLite.SQLiteParameter("@shortDescription", shortDescription);
            idbParam[23] = new System.Data.SQLite.SQLiteParameter("@sic", sic);
            idbParam[24] = new System.Data.SQLite.SQLiteParameter("@standarizedActive", standarizedActive);
            idbParam[25] = new System.Data.SQLite.SQLiteParameter("@state", state);
            idbParam[26] = new System.Data.SQLite.SQLiteParameter("@stockExchange", stockExchange);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("UPDATE Company SET template = @template, ticker = @ticker, active = @active, businessAddress = @businessAddress, businessPhoneNbr = @businessPhoneNbr, ceo = @ceo, cik = @cik, companyUrl = @companyUrl, country = @country, date_creation = @date_creation, employees = @employees, incCountry = @incCountry, incState = @incState, industryCategory = @industryCategory, industryGroup = @industryGroup, legalName = @legalName, lei = @lei, Description = @Description, mailingAddress = @mailingAddress, name = @name, sector = @sector, shortDescription = @shortDescription, sic = @sic, standarizedActive = @standarizedActive, state = @state, stockExchange = @stockExchange WHERE companyId = @companyId", idbParam);
        
            return rowsAffected;
        }
        
        public long Delete_Company(long  companyId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@companyId", companyId);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("DELETE FROM Company WHERE companyId = @companyId", idbParam);
        
            return rowsAffected;
        }
        
        public DataSet Select_Company(string companyId, string template, string ticker, string active, string businessAddress, string businessPhoneNbr, string ceo, string cik, string companyUrl, string country, string date_creationFrom, string date_creationTo, string employees, string incCountry, string incState, string industryCategory, string industryGroup, string legalName, string lei, string Description, string mailingAddress, string name, string sector, string shortDescription, string sic, string standarizedActive, string state, string stockExchange, string sortingField)
        {
            int count = 0;
            if (String.IsNullOrEmpty(companyId) == false) ++count;
            if (String.IsNullOrEmpty(template) == false) ++count;
            if (String.IsNullOrEmpty(ticker) == false) ++count;
            if (String.IsNullOrEmpty(active) == false) ++count;
            if (String.IsNullOrEmpty(businessAddress) == false) ++count;
            if (String.IsNullOrEmpty(businessPhoneNbr) == false) ++count;
            if (String.IsNullOrEmpty(ceo) == false) ++count;
            if (String.IsNullOrEmpty(cik) == false) ++count;
            if (String.IsNullOrEmpty(companyUrl) == false) ++count;
            if (String.IsNullOrEmpty(country) == false) ++count;
            if (String.IsNullOrEmpty(date_creationFrom) == false) ++count;
            if (String.IsNullOrEmpty(date_creationTo) == false) ++count;
            if (String.IsNullOrEmpty(employees) == false) ++count;
            if (String.IsNullOrEmpty(incCountry) == false) ++count;
            if (String.IsNullOrEmpty(incState) == false) ++count;
            if (String.IsNullOrEmpty(industryCategory) == false) ++count;
            if (String.IsNullOrEmpty(industryGroup) == false) ++count;
            if (String.IsNullOrEmpty(legalName) == false) ++count;
            if (String.IsNullOrEmpty(lei) == false) ++count;
            if (String.IsNullOrEmpty(Description) == false) ++count;
            if (String.IsNullOrEmpty(mailingAddress) == false) ++count;
            if (String.IsNullOrEmpty(name) == false) ++count;
            if (String.IsNullOrEmpty(sector) == false) ++count;
            if (String.IsNullOrEmpty(shortDescription) == false) ++count;
            if (String.IsNullOrEmpty(sic) == false) ++count;
            if (String.IsNullOrEmpty(standarizedActive) == false) ++count;
            if (String.IsNullOrEmpty(state) == false) ++count;
            if (String.IsNullOrEmpty(stockExchange) == false) ++count;
            ++count; // sorting field
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[count];
        
            int index = 0;
            if (String.IsNullOrEmpty(companyId) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@companyId", companyId);
                ++index;
            }
            if (String.IsNullOrEmpty(template) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@template", template);
                ++index;
            }
            if (String.IsNullOrEmpty(ticker) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@ticker", ticker);
                ++index;
            }
            if (String.IsNullOrEmpty(active) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@active", active);
                ++index;
            }
            if (String.IsNullOrEmpty(businessAddress) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@businessAddress", businessAddress);
                ++index;
            }
            if (String.IsNullOrEmpty(businessPhoneNbr) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@businessPhoneNbr", businessPhoneNbr);
                ++index;
            }
            if (String.IsNullOrEmpty(ceo) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@ceo", ceo);
                ++index;
            }
            if (String.IsNullOrEmpty(cik) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@cik", cik);
                ++index;
            }
            if (String.IsNullOrEmpty(companyUrl) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@companyUrl", companyUrl);
                ++index;
            }
            if (String.IsNullOrEmpty(country) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@country", country);
                ++index;
            }
            if (String.IsNullOrEmpty(date_creationFrom) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_creationFrom", date_creationFrom);
                ++index;
            }
            if (String.IsNullOrEmpty(date_creationTo) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_creationTo", date_creationTo);
                ++index;
            }
            if (String.IsNullOrEmpty(employees) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@employees", employees);
                ++index;
            }
            if (String.IsNullOrEmpty(incCountry) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@incCountry", incCountry);
                ++index;
            }
            if (String.IsNullOrEmpty(incState) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@incState", incState);
                ++index;
            }
            if (String.IsNullOrEmpty(industryCategory) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@industryCategory", industryCategory);
                ++index;
            }
            if (String.IsNullOrEmpty(industryGroup) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@industryGroup", industryGroup);
                ++index;
            }
            if (String.IsNullOrEmpty(legalName) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@legalName", legalName);
                ++index;
            }
            if (String.IsNullOrEmpty(lei) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@lei", lei);
                ++index;
            }
            if (String.IsNullOrEmpty(Description) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@Description", Description);
                ++index;
            }
            if (String.IsNullOrEmpty(mailingAddress) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@mailingAddress", mailingAddress);
                ++index;
            }
            if (String.IsNullOrEmpty(name) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@name", name);
                ++index;
            }
            if (String.IsNullOrEmpty(sector) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@sector", sector);
                ++index;
            }
            if (String.IsNullOrEmpty(shortDescription) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@shortDescription", shortDescription);
                ++index;
            }
            if (String.IsNullOrEmpty(sic) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@sic", sic);
                ++index;
            }
            if (String.IsNullOrEmpty(standarizedActive) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@standarizedActive", standarizedActive);
                ++index;
            }
            if (String.IsNullOrEmpty(state) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@state", state);
                ++index;
            }
            if (String.IsNullOrEmpty(stockExchange) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@stockExchange", stockExchange);
                ++index;
            }
            idbParam[index] = new System.Data.SQLite.SQLiteParameter("@sortingField", sortingField);
            string strSQLSentence = "SELECT * FROM Company WHERE ";
            bool firstLine = true;
            if (String.IsNullOrEmpty(companyId) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "companyId = @companyId";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND companyId = @companyId";
                }
            }
            if (String.IsNullOrEmpty(template) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "template = @template";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND template = @template";
                }
            }
            if (String.IsNullOrEmpty(ticker) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "ticker = @ticker";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND ticker = @ticker";
                }
            }
            if (String.IsNullOrEmpty(active) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "active = @active";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND active = @active";
                }
            }
            if (String.IsNullOrEmpty(businessAddress) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "businessAddress = @businessAddress";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND businessAddress = @businessAddress";
                }
            }
            if (String.IsNullOrEmpty(businessPhoneNbr) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "businessPhoneNbr = @businessPhoneNbr";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND businessPhoneNbr = @businessPhoneNbr";
                }
            }
            if (String.IsNullOrEmpty(ceo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "ceo = @ceo";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND ceo = @ceo";
                }
            }
            if (String.IsNullOrEmpty(cik) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "cik = @cik";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND cik = @cik";
                }
            }
            if (String.IsNullOrEmpty(companyUrl) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "companyUrl = @companyUrl";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND companyUrl = @companyUrl";
                }
            }
            if (String.IsNullOrEmpty(country) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "country = @country";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND country = @country";
                }
            }
            if (String.IsNullOrEmpty(date_creationFrom) == false && String.IsNullOrEmpty(date_creationTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_creation BETWEEN @date_creationFrom AND @date_creationTo";
                    firstLine = false;
                }
                else
                {
                   strSQLSentence += " AND date_creation BETWEEN @date_creationFrom AND @date_creationTo";
                }
            }
            else if (String.IsNullOrEmpty(date_creationFrom) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_creation >= @date_creationFrom";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_creation >= @date_creationFrom";
                }
            }
            else if (String.IsNullOrEmpty(date_creationTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_creation <= @date_creationTo";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_creation <= @date_creationTo";
                }
            }
            if (String.IsNullOrEmpty(employees) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "employees = @employees";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND employees = @employees";
                }
            }
            if (String.IsNullOrEmpty(incCountry) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "incCountry = @incCountry";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND incCountry = @incCountry";
                }
            }
            if (String.IsNullOrEmpty(incState) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "incState = @incState";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND incState = @incState";
                }
            }
            if (String.IsNullOrEmpty(industryCategory) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "industryCategory = @industryCategory";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND industryCategory = @industryCategory";
                }
            }
            if (String.IsNullOrEmpty(industryGroup) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "industryGroup = @industryGroup";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND industryGroup = @industryGroup";
                }
            }
            if (String.IsNullOrEmpty(legalName) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "legalName = @legalName";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND legalName = @legalName";
                }
            }
            if (String.IsNullOrEmpty(lei) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "lei = @lei";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND lei = @lei";
                }
            }
            if (String.IsNullOrEmpty(Description) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "Description = @Description";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND Description = @Description";
                }
            }
            if (String.IsNullOrEmpty(mailingAddress) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "mailingAddress = @mailingAddress";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND mailingAddress = @mailingAddress";
                }
            }
            if (String.IsNullOrEmpty(name) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "name = @name";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND name = @name";
                }
            }
            if (String.IsNullOrEmpty(sector) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "sector = @sector";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND sector = @sector";
                }
            }
            if (String.IsNullOrEmpty(shortDescription) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "shortDescription = @shortDescription";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND shortDescription = @shortDescription";
                }
            }
            if (String.IsNullOrEmpty(sic) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "sic = @sic";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND sic = @sic";
                }
            }
            if (String.IsNullOrEmpty(standarizedActive) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "standarizedActive = @standarizedActive";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND standarizedActive = @standarizedActive";
                }
            }
            if (String.IsNullOrEmpty(state) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "state = @state";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND state = @state";
                }
            }
            if (String.IsNullOrEmpty(stockExchange) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "stockExchange = @stockExchange";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND stockExchange = @stockExchange";
                }
            }
            strSQLSentence += " ORDER BY @sortingField";
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS(strSQLSentence, "Company", idbParam, ref ret);
        }
        
        public DataSet Get_By_Id_Company(long  companyId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@companyId", companyId);
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS("SELECT * FROM Company WHERE companyId = @companyId", "Company", idbParam, ref ret);
        }
    }
}
