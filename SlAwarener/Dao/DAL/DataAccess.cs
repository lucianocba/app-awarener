using System;
using System.Data;
using System.Data.SQLite;

namespace DAL
{

	public class DataAccess
	{
		
		/// <summary>
		/// The connection for the chosen DB engine.
		/// </summary>
		private IDbConnection cn = null;
		/// <summary>
		/// The transaction on the connection.
		/// </summary>
		private IDbTransaction transaction = null;

        private string connectionString = "";

		/// <summary>
		/// Constructor for DataAccess.
		/// </summary>
		public DataAccess()
		{
		}

        /// <summary>
        /// Creates a connection for accessing the DB.
        /// </summary>
        /// <param name="connectionString"></param>
		public void CreateConnection(string connectionStr)
        {
            connectionString = connectionStr;
            this.cn = new SQLiteConnection(connectionString);
        }

        /// <summary>
        /// Creates a connection for accessing the DB.
        /// </summary>
        public void CreateConnection()
		{
            if (String.IsNullOrEmpty(connectionString))
            {
                //connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
                connectionString = "Data Source=AwarenerInstance.db;Version=3;";
            }
           
            this.cn = new SQLiteConnection(connectionString);
		}

		/// <summary>
		/// Initiates a transaction.
		/// </summary>
		public void BeginTransaction()
		{
			try
			{
				this.cn.Open();
				this.transaction = this.cn.BeginTransaction();
			}
			catch(Exception e)
			{
				e.GetType().ToString();
			}
		}


		/// <summary>
		/// Rolls back the open transaction.
		/// </summary>
		public void RollbackTransaction()
		{
			try
			{
				this.transaction.Rollback();
			}
			catch(Exception e)
			{
				throw new DataException("Rollback Transaction failed. Error description: " + e.Message);
			}
			finally
			{
				this.cn.Close();
			}
		}

		/// <summary>
		/// Terminates the open transaction
		/// </summary>
		public void EndTransaction()
		{
			try
			{
				this.transaction.Commit();
				this.cn.Close();
			}
			catch(Exception)
			{
				try
				{
					this.RollbackTransaction();
				}
				catch(Exception e1)
				{
					e1.GetType().ToString();
				}
			}
			this.transaction = null;
		}

		/// <summary>
		/// Loads the command parameters
		/// </summary>
		/// <param name="arParms">IDbDataParameter[]</param>
		/// <param name="cmd">IDbCommand</param>
		private void PopulateCmdParams(IDbDataParameter[] arParms, ref IDbCommand cmd)
		{
			for(int i = 0; i < arParms.Length; i++)
			{
				cmd.Parameters.Add(arParms[i]);
			}
		}

		/// <summary>
		/// Loads the command parameters
		/// </summary>
		/// <param name="arParms">IDbDataParameter[]</param>
		/// <param name="cmd">IDbCommand</param>
		private void PopulateOutParams(ref IDbDataParameter[] arParms, IDbCommand cmd)
		{
			for(int i = 0; i < arParms.Length; i++)
			{
				if (arParms[i].Direction == ParameterDirection.Output)
				{
					IDbDataParameter temp = (IDbDataParameter)cmd.Parameters[((string)arParms[i].ParameterName)];
					arParms[i].Value = temp.Value;
				}
			}
		}

        /// <summary>
        /// Executes a SQL query, returning it in a Data Set.
        /// </summary>
        /// <param name="sqlSentence"></param>
        /// <param name="tableName"></param>
        /// <param name="arParms"></param>
        /// <param name="rowsReturned"></param>
        /// <returns></returns>
        public DataSet ExecSentenceSQL_DS(string sqlSentence, string tableName, IDbDataParameter[] arParms, ref int rowsReturned)
		{
            IDataAdapter da;
			IDbCommand cmd = null;
			bool isTransaction;
			DataSet dstResults = new DataSet();
			try
			{
                cmd = new SQLiteCommand(sqlSentence, (SQLiteConnection)cn);

                if (arParms != null)
                {
                    this.PopulateCmdParams(arParms, ref cmd);
                }

                if (this.cn.State != ConnectionState.Open)
				{
					this.cn.Open();
					isTransaction = false;
				}
				else
				{
					cmd.Transaction = this.transaction;
					isTransaction = true;
				}

                da = new SQLiteDataAdapter((SQLiteCommand)cmd);
                da.TableMappings.Add("Table", tableName);
		
				rowsReturned = da.Fill(dstResults); 

				if(!isTransaction)
				{
					this.cn.Close();
				}

                if (arParms != null)
                {
                    this.PopulateOutParams(ref arParms, cmd);
                }

            }
			catch(Exception dbEx)
			{
				throw new DataException("ExecSentenceSQL_DS failed. Error description: " + dbEx.Message);
			}
			finally
			{
				cmd.Dispose();
				da = null;
			}
			return dstResults;
		}

        /// <summary>
        /// Executes an SQL instruction.
        /// </summary>
        /// <param name="sqlSentence"></param>
        /// <param name="arParms"></param>
        /// <param name="getLastID"></param>
        /// <returns></returns>
        public long ExecSentenceSQL(string sqlSentence, IDbDataParameter[] arParms, bool getLastID = false)
		{
			
			bool isTransaction;
			IDbCommand cmd = null;
            long rowsAffected = 0;
			try
			{
                cmd = new SQLiteCommand(sqlSentence, (SQLiteConnection)cn);

                if (arParms != null)
                {
                    this.PopulateCmdParams(arParms, ref cmd);
                }

                if (this.cn.State != ConnectionState.Open)
				{
					this.cn.Open();
					isTransaction = false;
				}
				else
				{
					cmd.Transaction = this.transaction;
					isTransaction = true;
				}

				rowsAffected = cmd.ExecuteNonQuery(); 

                if (getLastID)
                {
                    rowsAffected = ((SQLiteConnection)cn).LastInsertRowId;
                }

				if(!isTransaction)
				{
					this.cn.Close();
				}

                if (arParms != null)
                {
                    this.PopulateOutParams(ref arParms, cmd);
                }
            }
			catch(Exception dbEx)
			{
				throw new DataException("ExecSentenceSQL failed. Error description: " + dbEx.Message);
			}
			finally
			{
				cmd.Dispose();
			}
			return rowsAffected;
		}
    }
}
