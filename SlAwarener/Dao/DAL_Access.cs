using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace DAL
{

    public class DAL_Access
    {

        public static string SUBMENUID = "subMenuId";
        public static string ROLEID = "roleId";

        public long Add_Access(long  subMenuId, long  roleId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[2];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@subMenuId", subMenuId);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@roleId", roleId);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("INSERT INTO Access(subMenuId, roleId) VALUES (@subMenuId, @roleId)", idbParam);
        
            return rowsAffected;
        }
        
        public long Update_Access(long  subMenuId, long  roleId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[2];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@subMenuId", subMenuId);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@roleId", roleId);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("UPDATE Access SET subMenuId = @subMenuId, roleId = @roleId WHERE ", idbParam);
        
            return rowsAffected;
        }
        
        public DataSet Select_Access(string subMenuId, string roleId, string sortingField)
        {
            int count = 0;
            if (String.IsNullOrEmpty(subMenuId) == false) ++count;
            if (String.IsNullOrEmpty(roleId) == false) ++count;
            ++count; // sorting field
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[count];
        
            int index = 0;
            if (String.IsNullOrEmpty(subMenuId) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@subMenuId", subMenuId);
                ++index;
            }
            if (String.IsNullOrEmpty(roleId) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@roleId", roleId);
                ++index;
            }
            idbParam[index] = new System.Data.SQLite.SQLiteParameter("@sortingField", sortingField);
            string strSQLSentence = "SELECT * FROM Access WHERE ";
            bool firstLine = true;
            if (String.IsNullOrEmpty(subMenuId) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "subMenuId = @subMenuId";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND subMenuId = @subMenuId";
                }
            }
            if (String.IsNullOrEmpty(roleId) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "roleId = @roleId";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND roleId = @roleId";
                }
            }
            strSQLSentence += " ORDER BY @sortingField";
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS(strSQLSentence, "Access", idbParam, ref ret);
        }
        
        public DataSet Get_By_Id_Access()
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[0];
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS("SELECT * FROM Access WHERE ", "Access", idbParam, ref ret);
        }
    }
}
