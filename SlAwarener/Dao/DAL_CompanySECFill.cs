using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace DAL
{

    public class DAL_CompanySECFill
    {

        public static string COMPANYSECFILLID = "companySECFillId";
        public static string COMPANYID = "companyId";
        public static string DATE_ACCEPTEDDATE = "date_acceptedDate";
        public static string ACTIVE = "active";
        public static string DATE_CREATION = "date_creation";
        public static string FILINGURL = "filingUrl";
        public static string DATE_FILLINGDATE = "date_fillingDate";
        public static string INSTANCEURL = "instanceUrl";
        public static string DATE_PERIODENDED = "date_periodEnded";
        public static string REPORTTYPE = "reportType";
        public static string REPORTURL = "reportUrl";
        public static string DATE_UPDATED = "date_updated";

        public long Add_CompanySECFill(long  companyId, DateTime  date_acceptedDate, long  active, DateTime  date_creation, string  filingUrl, DateTime  date_fillingDate, string  instanceUrl, DateTime  date_periodEnded, string  reportType, string  reportUrl, DateTime  date_updated)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[11];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@companyId", companyId);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@date_acceptedDate", date_acceptedDate);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@active", active);
            idbParam[3] = new System.Data.SQLite.SQLiteParameter("@date_creation", date_creation);
            idbParam[4] = new System.Data.SQLite.SQLiteParameter("@filingUrl", filingUrl);
            idbParam[5] = new System.Data.SQLite.SQLiteParameter("@date_fillingDate", date_fillingDate);
            idbParam[6] = new System.Data.SQLite.SQLiteParameter("@instanceUrl", instanceUrl);
            idbParam[7] = new System.Data.SQLite.SQLiteParameter("@date_periodEnded", date_periodEnded);
            idbParam[8] = new System.Data.SQLite.SQLiteParameter("@reportType", reportType);
            idbParam[9] = new System.Data.SQLite.SQLiteParameter("@reportUrl", reportUrl);
            idbParam[10] = new System.Data.SQLite.SQLiteParameter("@date_updated", date_updated);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long id = db.ExecSentenceSQL("INSERT INTO CompanySECFill(companyId, date_acceptedDate, active, date_creation, filingUrl, date_fillingDate, instanceUrl, date_periodEnded, reportType, reportUrl, date_updated) VALUES (@companyId, @date_acceptedDate, @active, @date_creation, @filingUrl, @date_fillingDate, @instanceUrl, @date_periodEnded, @reportType, @reportUrl, @date_updated)", idbParam, true);
            return id;
        }
        
        public long Update_CompanySECFill(long  companySECFillId, long  companyId, DateTime  date_acceptedDate, long  active, DateTime  date_creation, string  filingUrl, DateTime  date_fillingDate, string  instanceUrl, DateTime  date_periodEnded, string  reportType, string  reportUrl, DateTime  date_updated)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[12];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@companySECFillId", companySECFillId);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@companyId", companyId);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@date_acceptedDate", date_acceptedDate);
            idbParam[3] = new System.Data.SQLite.SQLiteParameter("@active", active);
            idbParam[4] = new System.Data.SQLite.SQLiteParameter("@date_creation", date_creation);
            idbParam[5] = new System.Data.SQLite.SQLiteParameter("@filingUrl", filingUrl);
            idbParam[6] = new System.Data.SQLite.SQLiteParameter("@date_fillingDate", date_fillingDate);
            idbParam[7] = new System.Data.SQLite.SQLiteParameter("@instanceUrl", instanceUrl);
            idbParam[8] = new System.Data.SQLite.SQLiteParameter("@date_periodEnded", date_periodEnded);
            idbParam[9] = new System.Data.SQLite.SQLiteParameter("@reportType", reportType);
            idbParam[10] = new System.Data.SQLite.SQLiteParameter("@reportUrl", reportUrl);
            idbParam[11] = new System.Data.SQLite.SQLiteParameter("@date_updated", date_updated);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("UPDATE CompanySECFill SET companyId = @companyId, date_acceptedDate = @date_acceptedDate, active = @active, date_creation = @date_creation, filingUrl = @filingUrl, date_fillingDate = @date_fillingDate, instanceUrl = @instanceUrl, date_periodEnded = @date_periodEnded, reportType = @reportType, reportUrl = @reportUrl, date_updated = @date_updated WHERE companySECFillId = @companySECFillId", idbParam);
        
            return rowsAffected;
        }
        
        public long Delete_CompanySECFill(long  companySECFillId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@companySECFillId", companySECFillId);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("DELETE FROM CompanySECFill WHERE companySECFillId = @companySECFillId", idbParam);
        
            return rowsAffected;
        }
        
        public DataSet Select_CompanySECFill(string companySECFillId, string companyId, string date_acceptedDateFrom, string date_acceptedDateTo, string active, string date_creationFrom, string date_creationTo, string filingUrl, string date_fillingDateFrom, string date_fillingDateTo, string instanceUrl, string date_periodEndedFrom, string date_periodEndedTo, string reportType, string reportUrl, string date_updatedFrom, string date_updatedTo, string sortingField)
        {
            int count = 0;
            if (String.IsNullOrEmpty(companySECFillId) == false) ++count;
            if (String.IsNullOrEmpty(companyId) == false) ++count;
            if (String.IsNullOrEmpty(date_acceptedDateFrom) == false) ++count;
            if (String.IsNullOrEmpty(date_acceptedDateTo) == false) ++count;
            if (String.IsNullOrEmpty(active) == false) ++count;
            if (String.IsNullOrEmpty(date_creationFrom) == false) ++count;
            if (String.IsNullOrEmpty(date_creationTo) == false) ++count;
            if (String.IsNullOrEmpty(filingUrl) == false) ++count;
            if (String.IsNullOrEmpty(date_fillingDateFrom) == false) ++count;
            if (String.IsNullOrEmpty(date_fillingDateTo) == false) ++count;
            if (String.IsNullOrEmpty(instanceUrl) == false) ++count;
            if (String.IsNullOrEmpty(date_periodEndedFrom) == false) ++count;
            if (String.IsNullOrEmpty(date_periodEndedTo) == false) ++count;
            if (String.IsNullOrEmpty(reportType) == false) ++count;
            if (String.IsNullOrEmpty(reportUrl) == false) ++count;
            if (String.IsNullOrEmpty(date_updatedFrom) == false) ++count;
            if (String.IsNullOrEmpty(date_updatedTo) == false) ++count;
            ++count; // sorting field
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[count];
        
            int index = 0;
            if (String.IsNullOrEmpty(companySECFillId) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@companySECFillId", companySECFillId);
                ++index;
            }
            if (String.IsNullOrEmpty(companyId) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@companyId", companyId);
                ++index;
            }
            if (String.IsNullOrEmpty(date_acceptedDateFrom) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_acceptedDateFrom", date_acceptedDateFrom);
                ++index;
            }
            if (String.IsNullOrEmpty(date_acceptedDateTo) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_acceptedDateTo", date_acceptedDateTo);
                ++index;
            }
            if (String.IsNullOrEmpty(active) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@active", active);
                ++index;
            }
            if (String.IsNullOrEmpty(date_creationFrom) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_creationFrom", date_creationFrom);
                ++index;
            }
            if (String.IsNullOrEmpty(date_creationTo) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_creationTo", date_creationTo);
                ++index;
            }
            if (String.IsNullOrEmpty(filingUrl) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@filingUrl", filingUrl);
                ++index;
            }
            if (String.IsNullOrEmpty(date_fillingDateFrom) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_fillingDateFrom", date_fillingDateFrom);
                ++index;
            }
            if (String.IsNullOrEmpty(date_fillingDateTo) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_fillingDateTo", date_fillingDateTo);
                ++index;
            }
            if (String.IsNullOrEmpty(instanceUrl) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@instanceUrl", instanceUrl);
                ++index;
            }
            if (String.IsNullOrEmpty(date_periodEndedFrom) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_periodEndedFrom", date_periodEndedFrom);
                ++index;
            }
            if (String.IsNullOrEmpty(date_periodEndedTo) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_periodEndedTo", date_periodEndedTo);
                ++index;
            }
            if (String.IsNullOrEmpty(reportType) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@reportType", reportType);
                ++index;
            }
            if (String.IsNullOrEmpty(reportUrl) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@reportUrl", reportUrl);
                ++index;
            }
            if (String.IsNullOrEmpty(date_updatedFrom) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_updatedFrom", date_updatedFrom);
                ++index;
            }
            if (String.IsNullOrEmpty(date_updatedTo) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_updatedTo", date_updatedTo);
                ++index;
            }
            idbParam[index] = new System.Data.SQLite.SQLiteParameter("@sortingField", sortingField);
            string strSQLSentence = "SELECT * FROM CompanySECFill WHERE ";
            bool firstLine = true;
            if (String.IsNullOrEmpty(companySECFillId) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "companySECFillId = @companySECFillId";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND companySECFillId = @companySECFillId";
                }
            }
            if (String.IsNullOrEmpty(companyId) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "companyId = @companyId";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND companyId = @companyId";
                }
            }
            if (String.IsNullOrEmpty(date_acceptedDateFrom) == false && String.IsNullOrEmpty(date_acceptedDateTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_acceptedDate BETWEEN @date_acceptedDateFrom AND @date_acceptedDateTo";
                    firstLine = false;
                }
                else
                {
                   strSQLSentence += " AND date_acceptedDate BETWEEN @date_acceptedDateFrom AND @date_acceptedDateTo";
                }
            }
            else if (String.IsNullOrEmpty(date_acceptedDateFrom) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_acceptedDate >= @date_acceptedDateFrom";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_acceptedDate >= @date_acceptedDateFrom";
                }
            }
            else if (String.IsNullOrEmpty(date_acceptedDateTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_acceptedDate <= @date_acceptedDateTo";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_acceptedDate <= @date_acceptedDateTo";
                }
            }
            if (String.IsNullOrEmpty(active) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "active = @active";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND active = @active";
                }
            }
            if (String.IsNullOrEmpty(date_creationFrom) == false && String.IsNullOrEmpty(date_creationTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_creation BETWEEN @date_creationFrom AND @date_creationTo";
                    firstLine = false;
                }
                else
                {
                   strSQLSentence += " AND date_creation BETWEEN @date_creationFrom AND @date_creationTo";
                }
            }
            else if (String.IsNullOrEmpty(date_creationFrom) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_creation >= @date_creationFrom";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_creation >= @date_creationFrom";
                }
            }
            else if (String.IsNullOrEmpty(date_creationTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_creation <= @date_creationTo";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_creation <= @date_creationTo";
                }
            }
            if (String.IsNullOrEmpty(filingUrl) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "filingUrl = @filingUrl";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND filingUrl = @filingUrl";
                }
            }
            if (String.IsNullOrEmpty(date_fillingDateFrom) == false && String.IsNullOrEmpty(date_fillingDateTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_fillingDate BETWEEN @date_fillingDateFrom AND @date_fillingDateTo";
                    firstLine = false;
                }
                else
                {
                   strSQLSentence += " AND date_fillingDate BETWEEN @date_fillingDateFrom AND @date_fillingDateTo";
                }
            }
            else if (String.IsNullOrEmpty(date_fillingDateFrom) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_fillingDate >= @date_fillingDateFrom";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_fillingDate >= @date_fillingDateFrom";
                }
            }
            else if (String.IsNullOrEmpty(date_fillingDateTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_fillingDate <= @date_fillingDateTo";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_fillingDate <= @date_fillingDateTo";
                }
            }
            if (String.IsNullOrEmpty(instanceUrl) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "instanceUrl = @instanceUrl";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND instanceUrl = @instanceUrl";
                }
            }
            if (String.IsNullOrEmpty(date_periodEndedFrom) == false && String.IsNullOrEmpty(date_periodEndedTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_periodEnded BETWEEN @date_periodEndedFrom AND @date_periodEndedTo";
                    firstLine = false;
                }
                else
                {
                   strSQLSentence += " AND date_periodEnded BETWEEN @date_periodEndedFrom AND @date_periodEndedTo";
                }
            }
            else if (String.IsNullOrEmpty(date_periodEndedFrom) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_periodEnded >= @date_periodEndedFrom";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_periodEnded >= @date_periodEndedFrom";
                }
            }
            else if (String.IsNullOrEmpty(date_periodEndedTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_periodEnded <= @date_periodEndedTo";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_periodEnded <= @date_periodEndedTo";
                }
            }
            if (String.IsNullOrEmpty(reportType) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "reportType = @reportType";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND reportType = @reportType";
                }
            }
            if (String.IsNullOrEmpty(reportUrl) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "reportUrl = @reportUrl";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND reportUrl = @reportUrl";
                }
            }
            if (String.IsNullOrEmpty(date_updatedFrom) == false && String.IsNullOrEmpty(date_updatedTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_updated BETWEEN @date_updatedFrom AND @date_updatedTo";
                    firstLine = false;
                }
                else
                {
                   strSQLSentence += " AND date_updated BETWEEN @date_updatedFrom AND @date_updatedTo";
                }
            }
            else if (String.IsNullOrEmpty(date_updatedFrom) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_updated >= @date_updatedFrom";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_updated >= @date_updatedFrom";
                }
            }
            else if (String.IsNullOrEmpty(date_updatedTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_updated <= @date_updatedTo";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_updated <= @date_updatedTo";
                }
            }
            strSQLSentence += " ORDER BY @sortingField";
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS(strSQLSentence, "CompanySECFill", idbParam, ref ret);
        }
        
        public DataSet Get_By_Id_CompanySECFill(long  companySECFillId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@companySECFillId", companySECFillId);
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS("SELECT * FROM CompanySECFill WHERE companySECFillId = @companySECFillId", "CompanySECFill", idbParam, ref ret);
        }
    }
}
