using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace DAL
{

    public class DAL_CompanyNews
    {

        public static string COMPANYNEWSID = "companyNewsId";
        public static string COMPANYID = "companyId";
        public static string ACTIVE = "active";
        public static string DATE_CREATION = "date_creation";
        public static string DATE_PUBLICATIONDATE = "date_publicationDate";
        public static string SUMMARY = "summary";
        public static string TITLE = "title";
        public static string DATE_UPDATED = "date_updated";
        public static string URL = "url";

        public long Add_CompanyNews(long  companyId, long  active, DateTime  date_creation, DateTime  date_publicationDate, string  summary, string  title, DateTime  date_updated, string  url)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[8];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@companyId", companyId);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@active", active);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@date_creation", date_creation);
            idbParam[3] = new System.Data.SQLite.SQLiteParameter("@date_publicationDate", date_publicationDate);
            idbParam[4] = new System.Data.SQLite.SQLiteParameter("@summary", summary);
            idbParam[5] = new System.Data.SQLite.SQLiteParameter("@title", title);
            idbParam[6] = new System.Data.SQLite.SQLiteParameter("@date_updated", date_updated);
            idbParam[7] = new System.Data.SQLite.SQLiteParameter("@url", url);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long id = db.ExecSentenceSQL("INSERT INTO CompanyNews(companyId, active, date_creation, date_publicationDate, summary, title, date_updated, url) VALUES (@companyId, @active, @date_creation, @date_publicationDate, @summary, @title, @date_updated, @url)", idbParam, true);
            return id;
        }
        
        public long Update_CompanyNews(long  companyNewsId, long  companyId, long  active, DateTime  date_creation, DateTime  date_publicationDate, string  summary, string  title, DateTime  date_updated, string  url)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[9];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@companyNewsId", companyNewsId);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@companyId", companyId);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@active", active);
            idbParam[3] = new System.Data.SQLite.SQLiteParameter("@date_creation", date_creation);
            idbParam[4] = new System.Data.SQLite.SQLiteParameter("@date_publicationDate", date_publicationDate);
            idbParam[5] = new System.Data.SQLite.SQLiteParameter("@summary", summary);
            idbParam[6] = new System.Data.SQLite.SQLiteParameter("@title", title);
            idbParam[7] = new System.Data.SQLite.SQLiteParameter("@date_updated", date_updated);
            idbParam[8] = new System.Data.SQLite.SQLiteParameter("@url", url);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("UPDATE CompanyNews SET companyId = @companyId, active = @active, date_creation = @date_creation, date_publicationDate = @date_publicationDate, summary = @summary, title = @title, date_updated = @date_updated, url = @url WHERE companyNewsId = @companyNewsId", idbParam);
        
            return rowsAffected;
        }
        
        public long Delete_CompanyNews(long  companyNewsId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@companyNewsId", companyNewsId);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("DELETE FROM CompanyNews WHERE companyNewsId = @companyNewsId", idbParam);
        
            return rowsAffected;
        }
        
        public DataSet Select_CompanyNews(string companyNewsId, string companyId, string active, string date_creationFrom, string date_creationTo, string date_publicationDateFrom, string date_publicationDateTo, string summary, string title, string date_updatedFrom, string date_updatedTo, string url, string sortingField)
        {
            int count = 0;
            if (String.IsNullOrEmpty(companyNewsId) == false) ++count;
            if (String.IsNullOrEmpty(companyId) == false) ++count;
            if (String.IsNullOrEmpty(active) == false) ++count;
            if (String.IsNullOrEmpty(date_creationFrom) == false) ++count;
            if (String.IsNullOrEmpty(date_creationTo) == false) ++count;
            if (String.IsNullOrEmpty(date_publicationDateFrom) == false) ++count;
            if (String.IsNullOrEmpty(date_publicationDateTo) == false) ++count;
            if (String.IsNullOrEmpty(summary) == false) ++count;
            if (String.IsNullOrEmpty(title) == false) ++count;
            if (String.IsNullOrEmpty(date_updatedFrom) == false) ++count;
            if (String.IsNullOrEmpty(date_updatedTo) == false) ++count;
            if (String.IsNullOrEmpty(url) == false) ++count;
            ++count; // sorting field
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[count];
        
            int index = 0;
            if (String.IsNullOrEmpty(companyNewsId) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@companyNewsId", companyNewsId);
                ++index;
            }
            if (String.IsNullOrEmpty(companyId) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@companyId", companyId);
                ++index;
            }
            if (String.IsNullOrEmpty(active) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@active", active);
                ++index;
            }
            if (String.IsNullOrEmpty(date_creationFrom) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_creationFrom", date_creationFrom);
                ++index;
            }
            if (String.IsNullOrEmpty(date_creationTo) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_creationTo", date_creationTo);
                ++index;
            }
            if (String.IsNullOrEmpty(date_publicationDateFrom) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_publicationDateFrom", date_publicationDateFrom);
                ++index;
            }
            if (String.IsNullOrEmpty(date_publicationDateTo) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_publicationDateTo", date_publicationDateTo);
                ++index;
            }
            if (String.IsNullOrEmpty(summary) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@summary", summary);
                ++index;
            }
            if (String.IsNullOrEmpty(title) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@title", title);
                ++index;
            }
            if (String.IsNullOrEmpty(date_updatedFrom) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_updatedFrom", date_updatedFrom);
                ++index;
            }
            if (String.IsNullOrEmpty(date_updatedTo) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_updatedTo", date_updatedTo);
                ++index;
            }
            if (String.IsNullOrEmpty(url) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@url", url);
                ++index;
            }
            idbParam[index] = new System.Data.SQLite.SQLiteParameter("@sortingField", sortingField);
            string strSQLSentence = "SELECT * FROM CompanyNews WHERE ";
            bool firstLine = true;
            if (String.IsNullOrEmpty(companyNewsId) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "companyNewsId = @companyNewsId";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND companyNewsId = @companyNewsId";
                }
            }
            if (String.IsNullOrEmpty(companyId) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "companyId = @companyId";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND companyId = @companyId";
                }
            }
            if (String.IsNullOrEmpty(active) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "active = @active";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND active = @active";
                }
            }
            if (String.IsNullOrEmpty(date_creationFrom) == false && String.IsNullOrEmpty(date_creationTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_creation BETWEEN @date_creationFrom AND @date_creationTo";
                    firstLine = false;
                }
                else
                {
                   strSQLSentence += " AND date_creation BETWEEN @date_creationFrom AND @date_creationTo";
                }
            }
            else if (String.IsNullOrEmpty(date_creationFrom) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_creation >= @date_creationFrom";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_creation >= @date_creationFrom";
                }
            }
            else if (String.IsNullOrEmpty(date_creationTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_creation <= @date_creationTo";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_creation <= @date_creationTo";
                }
            }
            if (String.IsNullOrEmpty(date_publicationDateFrom) == false && String.IsNullOrEmpty(date_publicationDateTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_publicationDate BETWEEN @date_publicationDateFrom AND @date_publicationDateTo";
                    firstLine = false;
                }
                else
                {
                   strSQLSentence += " AND date_publicationDate BETWEEN @date_publicationDateFrom AND @date_publicationDateTo";
                }
            }
            else if (String.IsNullOrEmpty(date_publicationDateFrom) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_publicationDate >= @date_publicationDateFrom";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_publicationDate >= @date_publicationDateFrom";
                }
            }
            else if (String.IsNullOrEmpty(date_publicationDateTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_publicationDate <= @date_publicationDateTo";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_publicationDate <= @date_publicationDateTo";
                }
            }
            if (String.IsNullOrEmpty(summary) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "summary = @summary";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND summary = @summary";
                }
            }
            if (String.IsNullOrEmpty(title) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "title = @title";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND title = @title";
                }
            }
            if (String.IsNullOrEmpty(date_updatedFrom) == false && String.IsNullOrEmpty(date_updatedTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_updated BETWEEN @date_updatedFrom AND @date_updatedTo";
                    firstLine = false;
                }
                else
                {
                   strSQLSentence += " AND date_updated BETWEEN @date_updatedFrom AND @date_updatedTo";
                }
            }
            else if (String.IsNullOrEmpty(date_updatedFrom) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_updated >= @date_updatedFrom";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_updated >= @date_updatedFrom";
                }
            }
            else if (String.IsNullOrEmpty(date_updatedTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_updated <= @date_updatedTo";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_updated <= @date_updatedTo";
                }
            }
            if (String.IsNullOrEmpty(url) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "url = @url";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND url = @url";
                }
            }
            strSQLSentence += " ORDER BY @sortingField";
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS(strSQLSentence, "CompanyNews", idbParam, ref ret);
        }
        
        public DataSet Get_By_Id_CompanyNews(long  companyNewsId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@companyNewsId", companyNewsId);
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS("SELECT * FROM CompanyNews WHERE companyNewsId = @companyNewsId", "CompanyNews", idbParam, ref ret);
        }
    }
}
