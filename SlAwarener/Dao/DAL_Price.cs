using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace DAL
{

    public class DAL_Price
    {

        public static string PRICEID = "priceId";
        public static string COMPANYID = "companyId";
        public static string ACTIVE = "active";
        public static string ADJCLOSE = "adjClose";
        public static string ADJHIGH = "adjHigh";
        public static string ADJLOW = "adjLow";
        public static string ADJOPEN = "adjOpen";
        public static string ADJVOLUME = "adjVolume";
        public static string CLOSING = "closing";
        public static string DATE_CREATION = "date_creation";
        public static string DATE_DATE = "date_date";
        public static string EXDIVIDEND = "exDividend";
        public static string HIGH = "high";
        public static string LOW = "low";
        public static string OPENING = "opening";
        public static string SPLITRATIO = "splitRatio";
        public static string DATE_UPDATED = "date_updated";
        public static string VOLUME = "volume";

        public long Add_Price(long  companyId, long  active, decimal  adjClose, decimal  adjHigh, decimal  adjLow, decimal  adjOpen, decimal  adjVolume, decimal  closing, DateTime  date_creation, DateTime  date_date, decimal  exDividend, decimal  high, decimal  low, decimal  opening, decimal  splitRatio, DateTime  date_updated, decimal  volume)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[17];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@companyId", companyId);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@active", active);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@adjClose", adjClose);
            idbParam[3] = new System.Data.SQLite.SQLiteParameter("@adjHigh", adjHigh);
            idbParam[4] = new System.Data.SQLite.SQLiteParameter("@adjLow", adjLow);
            idbParam[5] = new System.Data.SQLite.SQLiteParameter("@adjOpen", adjOpen);
            idbParam[6] = new System.Data.SQLite.SQLiteParameter("@adjVolume", adjVolume);
            idbParam[7] = new System.Data.SQLite.SQLiteParameter("@closing", closing);
            idbParam[8] = new System.Data.SQLite.SQLiteParameter("@date_creation", date_creation);
            idbParam[9] = new System.Data.SQLite.SQLiteParameter("@date_date", date_date);
            idbParam[10] = new System.Data.SQLite.SQLiteParameter("@exDividend", exDividend);
            idbParam[11] = new System.Data.SQLite.SQLiteParameter("@high", high);
            idbParam[12] = new System.Data.SQLite.SQLiteParameter("@low", low);
            idbParam[13] = new System.Data.SQLite.SQLiteParameter("@opening", opening);
            idbParam[14] = new System.Data.SQLite.SQLiteParameter("@splitRatio", splitRatio);
            idbParam[15] = new System.Data.SQLite.SQLiteParameter("@date_updated", date_updated);
            idbParam[16] = new System.Data.SQLite.SQLiteParameter("@volume", volume);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long id = db.ExecSentenceSQL("INSERT INTO Price(companyId, active, adjClose, adjHigh, adjLow, adjOpen, adjVolume, closing, date_creation, date_date, exDividend, high, low, opening, splitRatio, date_updated, volume) VALUES (@companyId, @active, @adjClose, @adjHigh, @adjLow, @adjOpen, @adjVolume, @closing, @date_creation, @date_date, @exDividend, @high, @low, @opening, @splitRatio, @date_updated, @volume)", idbParam, true);
            return id;
        }
        
        public long Update_Price(long  priceId, long  companyId, long  active, decimal  adjClose, decimal  adjHigh, decimal  adjLow, decimal  adjOpen, decimal  adjVolume, decimal  closing, DateTime  date_creation, DateTime  date_date, decimal  exDividend, decimal  high, decimal  low, decimal  opening, decimal  splitRatio, DateTime  date_updated, decimal  volume)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[18];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@priceId", priceId);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@companyId", companyId);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@active", active);
            idbParam[3] = new System.Data.SQLite.SQLiteParameter("@adjClose", adjClose);
            idbParam[4] = new System.Data.SQLite.SQLiteParameter("@adjHigh", adjHigh);
            idbParam[5] = new System.Data.SQLite.SQLiteParameter("@adjLow", adjLow);
            idbParam[6] = new System.Data.SQLite.SQLiteParameter("@adjOpen", adjOpen);
            idbParam[7] = new System.Data.SQLite.SQLiteParameter("@adjVolume", adjVolume);
            idbParam[8] = new System.Data.SQLite.SQLiteParameter("@closing", closing);
            idbParam[9] = new System.Data.SQLite.SQLiteParameter("@date_creation", date_creation);
            idbParam[10] = new System.Data.SQLite.SQLiteParameter("@date_date", date_date);
            idbParam[11] = new System.Data.SQLite.SQLiteParameter("@exDividend", exDividend);
            idbParam[12] = new System.Data.SQLite.SQLiteParameter("@high", high);
            idbParam[13] = new System.Data.SQLite.SQLiteParameter("@low", low);
            idbParam[14] = new System.Data.SQLite.SQLiteParameter("@opening", opening);
            idbParam[15] = new System.Data.SQLite.SQLiteParameter("@splitRatio", splitRatio);
            idbParam[16] = new System.Data.SQLite.SQLiteParameter("@date_updated", date_updated);
            idbParam[17] = new System.Data.SQLite.SQLiteParameter("@volume", volume);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("UPDATE Price SET companyId = @companyId, active = @active, adjClose = @adjClose, adjHigh = @adjHigh, adjLow = @adjLow, adjOpen = @adjOpen, adjVolume = @adjVolume, closing = @closing, date_creation = @date_creation, date_date = @date_date, exDividend = @exDividend, high = @high, low = @low, opening = @opening, splitRatio = @splitRatio, date_updated = @date_updated, volume = @volume WHERE priceId = @priceId", idbParam);
        
            return rowsAffected;
        }
        
        public long Delete_Price(long  priceId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@priceId", priceId);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("DELETE FROM Price WHERE priceId = @priceId", idbParam);
        
            return rowsAffected;
        }
        
        public DataSet Select_Price(string priceId, string companyId, string active, string adjClose, string adjHigh, string adjLow, string adjOpen, string adjVolume, string closing, string date_creationFrom, string date_creationTo, string date_dateFrom, string date_dateTo, string exDividend, string high, string low, string opening, string splitRatio, string date_updatedFrom, string date_updatedTo, string volume, string sortingField)
        {
            int count = 0;
            if (String.IsNullOrEmpty(priceId) == false) ++count;
            if (String.IsNullOrEmpty(companyId) == false) ++count;
            if (String.IsNullOrEmpty(active) == false) ++count;
            if (String.IsNullOrEmpty(adjClose) == false) ++count;
            if (String.IsNullOrEmpty(adjHigh) == false) ++count;
            if (String.IsNullOrEmpty(adjLow) == false) ++count;
            if (String.IsNullOrEmpty(adjOpen) == false) ++count;
            if (String.IsNullOrEmpty(adjVolume) == false) ++count;
            if (String.IsNullOrEmpty(closing) == false) ++count;
            if (String.IsNullOrEmpty(date_creationFrom) == false) ++count;
            if (String.IsNullOrEmpty(date_creationTo) == false) ++count;
            if (String.IsNullOrEmpty(date_dateFrom) == false) ++count;
            if (String.IsNullOrEmpty(date_dateTo) == false) ++count;
            if (String.IsNullOrEmpty(exDividend) == false) ++count;
            if (String.IsNullOrEmpty(high) == false) ++count;
            if (String.IsNullOrEmpty(low) == false) ++count;
            if (String.IsNullOrEmpty(opening) == false) ++count;
            if (String.IsNullOrEmpty(splitRatio) == false) ++count;
            if (String.IsNullOrEmpty(date_updatedFrom) == false) ++count;
            if (String.IsNullOrEmpty(date_updatedTo) == false) ++count;
            if (String.IsNullOrEmpty(volume) == false) ++count;
            ++count; // sorting field
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[count];
        
            int index = 0;
            if (String.IsNullOrEmpty(priceId) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@priceId", priceId);
                ++index;
            }
            if (String.IsNullOrEmpty(companyId) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@companyId", companyId);
                ++index;
            }
            if (String.IsNullOrEmpty(active) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@active", active);
                ++index;
            }
            if (String.IsNullOrEmpty(adjClose) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@adjClose", adjClose);
                ++index;
            }
            if (String.IsNullOrEmpty(adjHigh) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@adjHigh", adjHigh);
                ++index;
            }
            if (String.IsNullOrEmpty(adjLow) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@adjLow", adjLow);
                ++index;
            }
            if (String.IsNullOrEmpty(adjOpen) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@adjOpen", adjOpen);
                ++index;
            }
            if (String.IsNullOrEmpty(adjVolume) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@adjVolume", adjVolume);
                ++index;
            }
            if (String.IsNullOrEmpty(closing) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@closing", closing);
                ++index;
            }
            if (String.IsNullOrEmpty(date_creationFrom) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_creationFrom", date_creationFrom);
                ++index;
            }
            if (String.IsNullOrEmpty(date_creationTo) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_creationTo", date_creationTo);
                ++index;
            }
            if (String.IsNullOrEmpty(date_dateFrom) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_dateFrom", date_dateFrom);
                ++index;
            }
            if (String.IsNullOrEmpty(date_dateTo) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_dateTo", date_dateTo);
                ++index;
            }
            if (String.IsNullOrEmpty(exDividend) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@exDividend", exDividend);
                ++index;
            }
            if (String.IsNullOrEmpty(high) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@high", high);
                ++index;
            }
            if (String.IsNullOrEmpty(low) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@low", low);
                ++index;
            }
            if (String.IsNullOrEmpty(opening) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@opening", opening);
                ++index;
            }
            if (String.IsNullOrEmpty(splitRatio) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@splitRatio", splitRatio);
                ++index;
            }
            if (String.IsNullOrEmpty(date_updatedFrom) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_updatedFrom", date_updatedFrom);
                ++index;
            }
            if (String.IsNullOrEmpty(date_updatedTo) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_updatedTo", date_updatedTo);
                ++index;
            }
            if (String.IsNullOrEmpty(volume) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@volume", volume);
                ++index;
            }
            idbParam[index] = new System.Data.SQLite.SQLiteParameter("@sortingField", sortingField);
            string strSQLSentence = "SELECT * FROM Price WHERE ";
            bool firstLine = true;
            if (String.IsNullOrEmpty(priceId) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "priceId = @priceId";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND priceId = @priceId";
                }
            }
            if (String.IsNullOrEmpty(companyId) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "companyId = @companyId";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND companyId = @companyId";
                }
            }
            if (String.IsNullOrEmpty(active) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "active = @active";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND active = @active";
                }
            }
            if (String.IsNullOrEmpty(adjClose) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "adjClose = @adjClose";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND adjClose = @adjClose";
                }
            }
            if (String.IsNullOrEmpty(adjHigh) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "adjHigh = @adjHigh";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND adjHigh = @adjHigh";
                }
            }
            if (String.IsNullOrEmpty(adjLow) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "adjLow = @adjLow";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND adjLow = @adjLow";
                }
            }
            if (String.IsNullOrEmpty(adjOpen) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "adjOpen = @adjOpen";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND adjOpen = @adjOpen";
                }
            }
            if (String.IsNullOrEmpty(adjVolume) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "adjVolume = @adjVolume";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND adjVolume = @adjVolume";
                }
            }
            if (String.IsNullOrEmpty(closing) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "closing = @closing";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND closing = @closing";
                }
            }
            if (String.IsNullOrEmpty(date_creationFrom) == false && String.IsNullOrEmpty(date_creationTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_creation BETWEEN @date_creationFrom AND @date_creationTo";
                    firstLine = false;
                }
                else
                {
                   strSQLSentence += " AND date_creation BETWEEN @date_creationFrom AND @date_creationTo";
                }
            }
            else if (String.IsNullOrEmpty(date_creationFrom) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_creation >= @date_creationFrom";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_creation >= @date_creationFrom";
                }
            }
            else if (String.IsNullOrEmpty(date_creationTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_creation <= @date_creationTo";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_creation <= @date_creationTo";
                }
            }
            if (String.IsNullOrEmpty(date_dateFrom) == false && String.IsNullOrEmpty(date_dateTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_date BETWEEN @date_dateFrom AND @date_dateTo";
                    firstLine = false;
                }
                else
                {
                   strSQLSentence += " AND date_date BETWEEN @date_dateFrom AND @date_dateTo";
                }
            }
            else if (String.IsNullOrEmpty(date_dateFrom) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_date >= @date_dateFrom";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_date >= @date_dateFrom";
                }
            }
            else if (String.IsNullOrEmpty(date_dateTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_date <= @date_dateTo";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_date <= @date_dateTo";
                }
            }
            if (String.IsNullOrEmpty(exDividend) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "exDividend = @exDividend";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND exDividend = @exDividend";
                }
            }
            if (String.IsNullOrEmpty(high) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "high = @high";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND high = @high";
                }
            }
            if (String.IsNullOrEmpty(low) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "low = @low";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND low = @low";
                }
            }
            if (String.IsNullOrEmpty(opening) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "opening = @opening";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND opening = @opening";
                }
            }
            if (String.IsNullOrEmpty(splitRatio) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "splitRatio = @splitRatio";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND splitRatio = @splitRatio";
                }
            }
            if (String.IsNullOrEmpty(date_updatedFrom) == false && String.IsNullOrEmpty(date_updatedTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_updated BETWEEN @date_updatedFrom AND @date_updatedTo";
                    firstLine = false;
                }
                else
                {
                   strSQLSentence += " AND date_updated BETWEEN @date_updatedFrom AND @date_updatedTo";
                }
            }
            else if (String.IsNullOrEmpty(date_updatedFrom) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_updated >= @date_updatedFrom";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_updated >= @date_updatedFrom";
                }
            }
            else if (String.IsNullOrEmpty(date_updatedTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_updated <= @date_updatedTo";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_updated <= @date_updatedTo";
                }
            }
            if (String.IsNullOrEmpty(volume) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "volume = @volume";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND volume = @volume";
                }
            }
            strSQLSentence += " ORDER BY @sortingField";
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS(strSQLSentence, "Price", idbParam, ref ret);
        }
        
        public DataSet Get_By_Id_Price(long  priceId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@priceId", priceId);
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS("SELECT * FROM Price WHERE priceId = @priceId", "Price", idbParam, ref ret);
        }
    }
}
