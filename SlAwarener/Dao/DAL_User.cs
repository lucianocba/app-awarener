using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace DAL
{

    public class DAL_User
    {

        public static string USERID = "userId";
        public static string NAME = "name";
        public static string LASTNAME = "lastName";
        public static string COUNTRY = "country";
        public static string STATE = "state";
        public static string CITY = "city";
        public static string DATE_BIRTHDAY = "date_birthday";
        public static string PASSWORD = "password";
        public static string EMAIL = "email";
        public static string PROFESSION = "profession";

        public long Add_User(string  name, string  lastName, string  country, string  state, string  city, DateTime  date_birthday, string  password, string  email, string  profession)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[9];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@name", name);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@lastName", lastName);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@country", country);
            idbParam[3] = new System.Data.SQLite.SQLiteParameter("@state", state);
            idbParam[4] = new System.Data.SQLite.SQLiteParameter("@city", city);
            idbParam[5] = new System.Data.SQLite.SQLiteParameter("@date_birthday", date_birthday);
            idbParam[6] = new System.Data.SQLite.SQLiteParameter("@password", password);
            idbParam[7] = new System.Data.SQLite.SQLiteParameter("@email", email);
            idbParam[8] = new System.Data.SQLite.SQLiteParameter("@profession", profession);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long id = db.ExecSentenceSQL("INSERT INTO User(name, lastName, country, state, city, date_birthday, password, email, profession) VALUES (@name, @lastName, @country, @state, @city, @date_birthday, @password, @email, @profession)", idbParam, true);
            return id;
        }
        
        public long Update_User(long  userId, string  name, string  lastName, string  country, string  state, string  city, DateTime  date_birthday, string  password, string  email, string  profession)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[10];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@userId", userId);
            idbParam[1] = new System.Data.SQLite.SQLiteParameter("@name", name);
            idbParam[2] = new System.Data.SQLite.SQLiteParameter("@lastName", lastName);
            idbParam[3] = new System.Data.SQLite.SQLiteParameter("@country", country);
            idbParam[4] = new System.Data.SQLite.SQLiteParameter("@state", state);
            idbParam[5] = new System.Data.SQLite.SQLiteParameter("@city", city);
            idbParam[6] = new System.Data.SQLite.SQLiteParameter("@date_birthday", date_birthday);
            idbParam[7] = new System.Data.SQLite.SQLiteParameter("@password", password);
            idbParam[8] = new System.Data.SQLite.SQLiteParameter("@email", email);
            idbParam[9] = new System.Data.SQLite.SQLiteParameter("@profession", profession);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("UPDATE User SET name = @name, lastName = @lastName, country = @country, state = @state, city = @city, date_birthday = @date_birthday, password = @password, email = @email, profession = @profession WHERE userId = @userId", idbParam);
        
            return rowsAffected;
        }
        
        public long Delete_User(long  userId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@userId", userId);
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            long rowsAffected = db.ExecSentenceSQL("DELETE FROM User WHERE userId = @userId", idbParam);
        
            return rowsAffected;
        }
        
        public DataSet Select_User(string userId, string name, string lastName, string country, string state, string city, string date_birthdayFrom, string date_birthdayTo, string password, string email, string profession, string sortingField)
        {
            int count = 0;
            if (String.IsNullOrEmpty(userId) == false) ++count;
            if (String.IsNullOrEmpty(name) == false) ++count;
            if (String.IsNullOrEmpty(lastName) == false) ++count;
            if (String.IsNullOrEmpty(country) == false) ++count;
            if (String.IsNullOrEmpty(state) == false) ++count;
            if (String.IsNullOrEmpty(city) == false) ++count;
            if (String.IsNullOrEmpty(date_birthdayFrom) == false) ++count;
            if (String.IsNullOrEmpty(date_birthdayTo) == false) ++count;
            if (String.IsNullOrEmpty(password) == false) ++count;
            if (String.IsNullOrEmpty(email) == false) ++count;
            if (String.IsNullOrEmpty(profession) == false) ++count;
            ++count; // sorting field
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[count];
        
            int index = 0;
            if (String.IsNullOrEmpty(userId) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@userId", userId);
                ++index;
            }
            if (String.IsNullOrEmpty(name) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@name", name);
                ++index;
            }
            if (String.IsNullOrEmpty(lastName) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@lastName", lastName);
                ++index;
            }
            if (String.IsNullOrEmpty(country) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@country", country);
                ++index;
            }
            if (String.IsNullOrEmpty(state) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@state", state);
                ++index;
            }
            if (String.IsNullOrEmpty(city) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@city", city);
                ++index;
            }
            if (String.IsNullOrEmpty(date_birthdayFrom) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_birthdayFrom", date_birthdayFrom);
                ++index;
            }
            if (String.IsNullOrEmpty(date_birthdayTo) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@date_birthdayTo", date_birthdayTo);
                ++index;
            }
            if (String.IsNullOrEmpty(password) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@password", password);
                ++index;
            }
            if (String.IsNullOrEmpty(email) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@email", email);
                ++index;
            }
            if (String.IsNullOrEmpty(profession) == false) 
            {
                idbParam[index] = new System.Data.SQLite.SQLiteParameter("@profession", profession);
                ++index;
            }
            idbParam[index] = new System.Data.SQLite.SQLiteParameter("@sortingField", sortingField);
            string strSQLSentence = "SELECT * FROM User WHERE ";
            bool firstLine = true;
            if (String.IsNullOrEmpty(userId) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "userId = @userId";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND userId = @userId";
                }
            }
            if (String.IsNullOrEmpty(name) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "name = @name";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND name = @name";
                }
            }
            if (String.IsNullOrEmpty(lastName) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "lastName = @lastName";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND lastName = @lastName";
                }
            }
            if (String.IsNullOrEmpty(country) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "country = @country";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND country = @country";
                }
            }
            if (String.IsNullOrEmpty(state) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "state = @state";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND state = @state";
                }
            }
            if (String.IsNullOrEmpty(city) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "city = @city";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND city = @city";
                }
            }
            if (String.IsNullOrEmpty(date_birthdayFrom) == false && String.IsNullOrEmpty(date_birthdayTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_birthday BETWEEN @date_birthdayFrom AND @date_birthdayTo";
                    firstLine = false;
                }
                else
                {
                   strSQLSentence += " AND date_birthday BETWEEN @date_birthdayFrom AND @date_birthdayTo";
                }
            }
            else if (String.IsNullOrEmpty(date_birthdayFrom) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_birthday >= @date_birthdayFrom";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_birthday >= @date_birthdayFrom";
                }
            }
            else if (String.IsNullOrEmpty(date_birthdayTo) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "date_birthday <= @date_birthdayTo";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND date_birthday <= @date_birthdayTo";
                }
            }
            if (String.IsNullOrEmpty(password) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "password = @password";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND password = @password";
                }
            }
            if (String.IsNullOrEmpty(email) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "email = @email";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND email = @email";
                }
            }
            if (String.IsNullOrEmpty(profession) == false) 
            {
                if (firstLine)
                {
                    strSQLSentence += "profession = @profession";
                    firstLine = false;
                }
                else
                {
                    strSQLSentence += " AND profession = @profession";
                }
            }
            strSQLSentence += " ORDER BY @sortingField";
        
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS(strSQLSentence, "User", idbParam, ref ret);
        }
        
        public DataSet Get_By_Id_User(long  userId)
        {
            System.Data.IDbDataParameter[] idbParam = new System.Data.IDbDataParameter[1];
        
            idbParam[0] = new System.Data.SQLite.SQLiteParameter("@userId", userId);
            DataAccess db = new DataAccess();
            db.CreateConnection();
            int ret = 0;
            return db.ExecSentenceSQL_DS("SELECT * FROM User WHERE userId = @userId", "User", idbParam, ref ret);
        }
    }
}
