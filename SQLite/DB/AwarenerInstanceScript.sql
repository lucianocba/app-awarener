CREATE TABLE `Access` (
	`subMenuId`	INTEGER,
	`roleId`	INTEGER
);


CREATE TABLE `Company` (
	`companyId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`template`	TEXT,
	`ticker`	TEXT,
	`active`	INTEGER,
	`businessAddress`	TEXT,
	`businessPhoneNbr`	TEXT,
	`ceo`	TEXT,
	`cik`	TEXT,
	`companyUrl`	TEXT,
	`country`	TEXT,
	`date_creation`	TEXT,
	`employees`	INTEGER,
	`incCountry`	TEXT,
	`incState`	TEXT,
	`industryCategory`	TEXT,
	`industryGroup`	TEXT,
	`legalName`	TEXT,
	`lei`	TEXT,
	`Description`	TEXT,
	`mailingAddress`	TEXT,
	`name`	TEXT,
	`sector`	TEXT,
	`shortDescription`	TEXT,
	`sic`	TEXT,
	`standarizedActive`	INTEGER,
	`state`	TEXT,
	`stockExchange`	TEXT
);


CREATE TABLE `CompanyNews` (
	`companyNewsId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`companyId`	INTEGER,
	`active`	INTEGER,
	`date_creation`	TEXT,
	`date_publicationDate`	TEXT,
	`summary`	TEXT,
	`title`	TEXT,
	`date_updated`	TEXT,
	`url`	TEXT
);


CREATE TABLE `CompanySECFill` (
	`companySECFillId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`companyId`	INTEGER,
	`date_acceptedDate`	TEXT,
	`active`	INTEGER,
	`date_creation`	TEXT,
	`filingUrl`	TEXT,
	`date_fillingDate`	TEXT,
	`instanceUrl`	TEXT,
	`date_periodEnded`	TEXT,
	`reportType`	TEXT,
	`reportUrl`	TEXT,
	`date_updated`	TEXT
);


CREATE TABLE `Menu` (
	`menuId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT,
	`icon`	TEXT
);


CREATE TABLE `Price` (
	`priceId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`companyId`	INTEGER,
	`active`	INTEGER,
	`adjClose`	REAL,
	`adjHigh`	REAL,
	`adjLow`	REAL,
	`adjOpen`	REAL,
	`adjVolume`	REAL,
	`closing`	REAL,
	`date_creation`	TEXT,
	`date_date`	TEXT,
	`exDividend`	REAL,
	`high`	REAL,
	`low`	REAL,
	`opening`	REAL,
	`splitRatio`	REAL,
	`date_updated`	TEXT,
	`volume`	REAL
);


CREATE TABLE `Role` (
	`roleId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`roleName`	TEXT
);


CREATE TABLE `StandarizedData` (
	`standarizedDataId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`active`	INTEGER,
	`date_creation`	TEXT,
	`fiscalPeriod`	TEXT,
	`fiscalYear`	INTEGER,
	`idCompany`	INTEGER,
	`statement`	TEXT,
	`tag`	TEXT,
	`date_updated`	TEXT,
	`value`	REAL
);


CREATE TABLE `SubMenu` (
	`subMenuId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`menuId`	INTEGER,
	`name`	TEXT,
	`controller`	TEXT,
	`action`	TEXT
);


CREATE TABLE `Tag` (
	`tagId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT,
	`tag`	TEXT,
	`statementCode`	TEXT,
	`template`	TEXT,
	`balance`	TEXT,
	`type`	TEXT,
	`units`	TEXT
);


CREATE TABLE `User` (
	`userId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT,
	`lastName`	TEXT,
	`country`	TEXT,
	`state`	TEXT,
	`city`	TEXT,
	`date_birthday`	TEXT,
	`password`	TEXT,
	`email`	TEXT,
	`profession`	TEXT
);

