import Vue from 'vue'
import Router from 'vue-router'

import Starter from './views/Starter.vue'
import About from './views/About.vue'
import Login from './views/Login.vue'
import Profile from './views/Profile.vue'
import Home from './views/Home.vue'
import SignIn from './views/SignIn.vue'
import Plans from './views/Plans.vue'
import Company from './views/Company.vue'
import CompanyVsCompany from './views/CompanyVsCompany.vue'

import Private from './layouts/Private.vue'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/login',
      component: Login
    },
    {
      path: '/starter',
      component: Starter
    },
    {
      path: '/signIn',
      component: SignIn
    },
    {
      path: '/',
      component: Starter
    },
    {
      path: '/home',
      component: Private,
      children: [
        {
          path: '/about',
          component: About
        },
        {
          path: '/home',
          component: Home
        },
        {
          path: '/profile',
          component: Profile
        },
        {
          path: '/companies',
          component: Company
        },
        {
          path: '/plans',
          component: Plans
        },
        {
          path: '/companyVscompany',
          component: CompanyVsCompany
        }
      ]
    }
  ]
});

export default router